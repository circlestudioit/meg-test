<?php

class Aziende extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'aziende'; /* Nome della pagina */
        $this->table                = 'aziende';
        $this->table_i18n           = 'aziende_i18n';
        $this->module_galleries     = 'photogalleries';
        $this->subject              = 'Azienda';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            
            $crud->set_relation('annual_production', 'aziende_production', 'name_range',null,'id');

            $crud->set_field_upload('logo', $this->config->item('azienda_logo')); 
            $crud->set_field_upload('cover', $this->config->item('azienda_cover'));

            $crud->display_as('name','Nome');
            $crud->display_as('url','URL (interno)');
            $crud->display_as('published','Pubblicata');
            $crud->display_as('indirizzo','Indirizzo completo');
            $crud->display_as('telefono','Telefono');
            $crud->display_as('Fax','Fax');
            $crud->display_as('email','E-mail');
            $crud->display_as('website','Sito Web');
            $crud->display_as('logo','Logo');
            $crud->display_as('cover','Copertina');
            $crud->display_as('annual_production','Produzione Annua');
            $crud->display_as('foundation_year','Anno di Fondazione');

            
            /* Sostituisco la create originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_after_insert(array($this,'insert_this_language'));
            //$crud->callback_after_update(array($this,'insert_this_language'));
            
            $crud->callback_before_delete(array($this,'delete_this_language'));
            
            /* Aggiungo la bandierina per editare ogni lingua attiva */
            $array_language = $this->active_langs;
            for($i=0; $i<count($array_language); $i++) {
                $crud->add_action('Language'.$array_language[$i], base_url(IMAGES.$array_language[$i].'_flag.png'), base_url($this->module.'/language/'.$array_language[$i].'/edit').'/');
            }
            
            $crud->columns('name', 'published', 'indirizzo', 'email');   
            
            $crud->fields('name', 'url', 'published', 'indirizzo', 'telefono', 'fax', 'email', 'website','logo','cover', 'annual_production', 'foundation_year');
            

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
   
    
    /**
     * Author: Raffaele Rotondo
     * Responsability: gestisce la modifica della lingua di una pagina
     */
    
    function language($_lang) {
        $this->css = array("admin.css");
        
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        
        try {
            $crud = new grocery_CRUD();
            
            $crud->set_theme('flexigrid');
            $crud->set_table($this->table_i18n);
            $crud->set_subject('lingua '.$_lang);
            $crud->unset_back_to_list();
            $crud->unset_print();
            $crud->unset_export();

            
            $crud->unset_texteditor('meta_description');
            
            /* Imposto la primary key con id e lingua, per poter editare la lingua giusta */
            $crud->set_primary_key(array('id' => $record_num,'lang' => $_lang));
            
            /* Sostituisco la update originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_update(array($this,'update_this_language'));

            $crud->display_as('title','Nome');
            $crud->display_as('orari_apertura','Orari apertura');
            $crud->display_as('content','Descrizione'); 
            $crud->display_as('other_content','Mission e filosofia aziendale'); 
            $crud->display_as('meta_keywords','META Keywords');
            $crud->display_as('meta_description','META Description');
            
            $crud->edit_fields('title','orari_apertura', 'content', 'other_content', 'meta_keywords', 'meta_description');
            
            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            

//            $this->_list_output($output);

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function insert_this_language($post_array, $primary_key) {
        
        
        /* Recupero la lingua dall'indirizzo */
        //$last = $this->uri->total_segments();
        $languages = $this->config->item('active_langs');
        
        foreach($languages as $lang) {
            try {
                /* Controllo se la lingua è già esistente nel db */
                $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));

                /* Se c'è già devo solo aggiornarla */
                if($query->num_rows() > 0) {
                    $this->db->where('lang', $lang);
                    $this->db->update($this->table_i18n, array('id' => $primary_key, 'lang' => $lang, 'title' => $post_array['name']), array('id' => $primary_key));
                    return true;
                }
                /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
                else {
                        $data = array('id' => $primary_key, 'lang' => $lang);
                        $this->db->insert($this->table_i18n, $data);
                        $this->db->where('lang', $lang);
                        $this->db->update($this->table_i18n, array('title' => $post_array['name']), array('id' => $primary_key));
                    }
            }
            catch (Exception $ex) {
                return $ex;
            }
        }

    }
    
    function update_this_language($post_array, $primary_key) {
        /* Recupero la lingua dall'indirizzo */
        $last = $this->uri->total_segments();
        $lang = $this->uri->segment($last-2);
        
        
        try {
            /* Controllo se la lingua è già esistente nel db */
            $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));
            
            /* Se c'è già devo solo aggiornarla */
            if($query->num_rows() > 0) {
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
            else {
                $data = array('id' => $primary_key, 'lang' => $lang);
                $this->db->insert($this->table_i18n, $data);
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
            }
            
        } catch (Exception $ex) {
            return $ex;
        }

    }
    
    function delete_this_language($primary_key) {
            try {
                /* Controllo se la riga esiste */
                $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key));

                /* Se c'è già devo solo aggiornarla */
                if($query->num_rows() > 0) {
                    $this->db->delete($this->table_i18n, array('id' => $primary_key));
                }
            }
            catch (Exception $ex) {
                return $ex;
            }
    }
    
	
    function gallery() {
        $this->css = array("admin.css");
        
        $image_crud = new image_CRUD();

        $image_crud->set_primary_key_field('photo_id');
        $image_crud->set_url_field('image');

        $image_crud->set_table($this->module_galleries);

        $image_crud
                ->set_ordering_field('ord')
                ->set_image_path($this->config->item('photo_image'))
                ->set_relation_field('photogallery_id');
                //->set_slider_size(500, 500);


        $output = $image_crud->render();
     
        /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
        $data['output'] = $output->output;
        $this->output = $this->load->view('pages/gallery_list', $data , true);

        /* Tramite il render del template caricherò*/
        $this->render_crud_page();

    }
    
}

?>
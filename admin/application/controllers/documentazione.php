<?php

class Documentazione extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'documentazione';
        $this->table                = 'documentazione';
        $this->table_i18n           = 'documentazione_i18n';
        $this->module_categories    = 'documentazione_categories';
        $this->module_galleries     = 'photogalleries';
        $this->subject              = 'Documentazione';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            
            $crud->set_field_upload('download', $this->config->item('press_download'));
            $crud->set_relation('category_id', $this->module_categories, 'name');
			
            $crud->display_as('name','Nome');
            $crud->display_as('published','Pubblicata');
            $crud->display_as('category_id','Cartella');
            $crud->display_as('ord','Ordine');
            $crud->display_as('download','Allegato (PDF)');
            
            $crud->fields('name', 'published', 'category_id', 'ord', 'download');   
            $crud->columns('name', 'published', 'category_id', 'ord');
            

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
}

?>
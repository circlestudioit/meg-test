<?php

class Pages_contents extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'pages_contents';
        $this->table                = 'pages_content';
        $this->subject              = 'Content di pagina';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');    
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            
            $crud->display_as('content_name','Nome content'); 
            $crud->display_as('description','Descrizione');
            
            $crud->fields('content_name', 'description');
            $crud->columns('content_name', 'description');  
            

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò */
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    
}

?>
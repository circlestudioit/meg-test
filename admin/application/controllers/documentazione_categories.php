<?php

class Documentazione_categories extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'documentazione_categories';
        $this->table                = 'documentazione_categories';
        $this->table_i18n           = 'documentazione_categories_i18n';
        $this->subject              = 'Cartella documentazione';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
			
            $crud->display_as('name','Cartella');
            $crud->display_as('ord','Ordine');
                        
            $crud->fields('name', 'ord');
            
            $crud->columns('name', 'ord');
            
            /* Sostituisco la create originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_after_insert(array($this,'insert_this_language'));
            //$crud->callback_after_update(array($this,'insert_this_language'));
            
            $crud->callback_before_delete(array($this,'delete_this_language'));
            
            /* Aggiungo la bandierina per editare ogni lingua attiva */
            $array_language = $this->active_langs;
            for($i=0; $i<count($array_language); $i++) {
                $crud->add_action('Language'.$array_language[$i], base_url(IMAGES.$array_language[$i].'_flag.png'), base_url($this->module.'/language/'.$array_language[$i].'/edit').'/');
            }
            
            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    /**
     * Author: Raffaele Rotondo
     * Responsibility: gestisce la modifica della lingua di una pagina
     */
    
    function language($_lang) {
        $this->css = array("admin.css");
        
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        
        try {
            $crud = new grocery_CRUD();
            
            $crud->set_theme('flexigrid');
            $crud->set_table($this->table_i18n);
            $crud->set_subject('lingua '.$_lang);
            $crud->unset_back_to_list();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_texteditor('meta_description');
            
            /* Imposto la primary key con id e lingua, per poter editare la lingua giusta */
            $crud->set_primary_key(array('id' => $record_num,'lang' => $_lang));
            
            /* Sostituisco la update originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_update(array($this,'update_this_language'));

            $crud->display_as('name','Nome');                                   
            $crud->display_as('description','Descrizione');
            
            $crud->edit_fields('name', 'description');
            
            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            

//            $this->_list_output($output);

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function insert_this_language($post_array, $primary_key) {
        
        
        /* Recupero la lingua dall'indirizzo */
        //$last = $this->uri->total_segments();
        $languages = $this->config->item('active_langs');
        
        foreach($languages as $lang) {
            try {
                /* Controllo se la lingua è già esistente nel db */
                $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));

                /* Se c'è già devo solo aggiornarla */
                if($query->num_rows() > 0) {
                    $this->db->where('lang', $lang);
                    $this->db->update($this->table_i18n, array('id' => $primary_key, 'lang' => $lang, 'name' => $post_array['name']), array('id' => $primary_key));
                    return true;
                }
                /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
                else {
                        $data = array('id' => $primary_key, 'lang' => $lang);
                        $this->db->insert($this->table_i18n, $data);
                        $this->db->where('lang', $lang);
                        $this->db->update($this->table_i18n, array('name' => $post_array['name']), array('id' => $primary_key));
                    }
            }
            catch (Exception $ex) {
                return $ex;
            }
        }

    }
    
    function update_this_language($post_array, $primary_key) {
        
        /* Recupero la lingua dall'indirizzo */
        $last = $this->uri->total_segments();
        $lang = $this->uri->segment($last-2);
        
        try {
            /* Controllo se la lingua è già esistente nel db */
            $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));
            
            /* Se c'è già devo solo aggiornarla */
            if($query->num_rows() > 0) {
                
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
            else {
                
                $data = array('id' => $primary_key, 'lang' => $lang);
                $this->db->insert($this->table_i18n, $data);
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            
        } catch (Exception $ex) {
            return $ex;
        }

    }
    
    function delete_this_language($primary_key) {
            try {
                /* Controllo se la riga esiste */
                $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key));

                /* Se c'è già devo solo aggiornarla */
                if($query->num_rows() > 0) {
                    $this->db->delete($this->table_i18n, array('id' => $primary_key));
                }
            }
            catch (Exception $ex) {
                return $ex;
            }
    }

    
}

?>
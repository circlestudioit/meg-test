<?php

class Users extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'users';
        $this->table                = 'admins';
        //$this->table_i18n           = 'post_i18n';
//        $this->module_categories    = 'post_categories';
//        $this->module_galleries     = 'photogalleries';
        $this->subject              = 'Utenti';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            			
            $crud->display_as('last_name','Cognome');
            $crud->display_as('first_name','Nome');
            $crud->display_as('email','Email');
            $crud->display_as('username','Username');
            $crud->display_as('password','Password');
            $crud->display_as('language','Lingua');
            
            $crud->field_type('password', 'password');
            
            $crud->callback_edit_field('password',array($this,'set_password_input_to_empty'));
            $crud->callback_add_field('password',array($this,'set_password_input_to_empty'));
 
            $crud->callback_before_update(array($this,'encrypt_password_callback'));
            $crud->callback_before_insert(array($this,'encrypt_password_callback'));
            
            /* Aggiungo l'azione per editare le immagini di questo progetto */
            //$crud->add_action('Modifica le foto di questo progetto', base_url(IMAGES.'photo.png'), base_url().$this->module.'/gallery/');
            
            $crud->fields('last_name', 'first_name', 'email', 'username', 'password', 'language');   
            
            $crud->columns('last_name', 'first_name', 'email', 'username', 'language');
            

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function encrypt_password_callback($post_array, $primary_key) {
        //Encrypt password only if is not empty. Else don't change the password to an empty field
        if(!empty($post_array['password']))
        {
            $post_array['password'] = md5($post_array['password']);
        }
        else
        {
            unset($post_array['password']);
        }

      return $post_array;
    }
    
//    function encrypt_password_callback($post_array, $primary_key) {
//        $this->load->library('encrypt');
//
//        //Encrypt password only if is not empty. Else don't change the password to an empty field
//        if(!empty($post_array['password']))
//        {
//            $key = 'super-secret-key';
//            $post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
//        }
//        else
//        {
//            unset($post_array['password']);
//        }
//
//      return $post_array;
//    }
 
function set_password_input_to_empty() {
    return "<input type='password' name='password' value='' />";
}
    
}

?>
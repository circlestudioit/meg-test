<?php

class Locations extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'locations';
        $this->table                = 'locations';
        $this->table_i18n           = 'locations_i18n';
        $this->relation_table       = 'locations_and_services';
        $this->services             = 'services';
        $this->subject              = 'Locations';
        
        $this->module_galleries     = 'photogalleries';
        
//        $this->photos_i18n          = 'photos_i18n';
//        $this->gallery_template     = 'photogalleries_templates';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_texteditor('address');
            
            $crud->set_field_upload('thumb', $this->config->item('photo_thumb'));
            $crud->set_field_upload('image', $this->config->item('photo_image'));
            $crud->set_field_upload('menu_image', $this->config->item('photo_image'));
            $crud->set_field_upload('attachment', $this->config->item('attachments'));
            
            $crud->set_relation('menu_id','menu','name',array('is_item' => FALSE));
            $crud->set_relation('country_id', 'countries', 'country_name');
            
            // Ogni location ha uno o più servizi disponibili
            $crud->set_relation_n_n('location_id', $this->relation_table, $this->services, 'location_id', 'service_id', 'name', 'priority');
            
            $crud->set_relation('featured_pizzetta', 'products', 'name');
            $crud->set_relation('photogallery_id', $this->module_galleries, 'name');
            $crud->set_relation('extra_photogallery_id', $this->module_galleries, 'name');
            $crud->set_relation('staff_photogallery_id', $this->module_galleries, 'name');
            
            
            $crud->display_as('location_id','Servizi disponibili presso questa location');
            $crud->display_as('loc_name','Nome');
            $crud->display_as('published','Pubblicata');
            $crud->display_as('next_opening','Next Opening');
            $crud->display_as('is_active','Pizzeria in attività');
            $crud->display_as('country_id','Paese');
            $crud->display_as('address','Indirizzo');
            $crud->display_as('lat','Latitudine');
            $crud->display_as('long','Longitudine');
            $crud->display_as('thumb','Logo ristorante');
            $crud->display_as('image','Immagine ristorante');
            $crud->display_as('featured_pizzetta','Pizzetta esclusiva');            
            $crud->display_as('menu_image','Immagine del menu');
            $crud->display_as('menu_id','Menu della pagina');
            $crud->display_as('attachment','Menu PDF scaricabile');
            $crud->display_as('photogallery_id','Photogallery');
            $crud->display_as('extra_photogallery_id','Galleria PROMO');
            $crud->display_as('staff_photogallery_id','Galleria STAFF');
            
            
            $crud->display_as('facebook_url','Facebook URL<br />(http://www.facebook.com/indirizzo)');
            $crud->display_as('twitter_url','Twitter URL<br />(http://www.twitter.com/indirizzo)');
            $crud->display_as('instagram_url','Instagram URL<br />(http://www.instagram.com/indirizzo)');
            $crud->display_as('email_url','E-mail<br />(tondapescara@tonda.pizza)');
            
            /* Aggiungo l'azione per editare le immagini di questo progetto */
            //$crud->add_action('Modifica le foto di questa galleria', base_url(IMAGES.'photo.png'), base_url().$this->module.'/gallery/');
            
            /* Aggiungo la bandierina per editare ogni lingua attiva */
            $array_language = $this->active_langs;
            for($i=0; $i<count($array_language); $i++) {
                $crud->add_action('Language'.$array_language[$i], base_url(IMAGES.$array_language[$i].'_flag.png'), base_url($this->module.'/language/'.$array_language[$i].'/edit').'/');
            }
            //Campi di visualizzazione griglia
            $crud->columns('loc_name', 'published', 'next_opening', 'country_id', 'address');   
            
            //Campi di edit
            $crud->fields('loc_name', 'published', 'next_opening', 'is_active', 'country_id', 'address', 'location_id', 'menu_id', 'thumb', 
                          'image', 'featured_pizzetta', 'menu_image', 'attachment', 'photogallery_id', 'extra_photogallery_id',
                          'staff_photogallery_id', 'lat', 'long', 'facebook_url', 'twitter_url', 'instagram_url', 'email_url');
            

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
   
    
    /**
     * Author: Raffaele Rotondo
     * Responsability: gestisce la modifica della lingua di una pagina
     */
    
    function language($_lang) {
        $this->css = array("admin.css");
        
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        
        try {
            $crud = new grocery_CRUD();
            
            $crud->set_theme('flexigrid');
            $crud->set_table($this->table_i18n);
            $crud->set_subject('lingua '.$_lang);
            $crud->unset_back_to_list();
            $crud->unset_print();
            $crud->unset_export();
            
            /* Imposto la primary key con id e lingua, per poter editare la lingua giusta */
            $crud->set_primary_key(array('id' => $record_num,'lang' => $_lang));
            
            /* Sostituisco la update originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_update(array($this,'update_this_language'));

            $crud->display_as('store_name','Nome location');
            $crud->display_as('store_short_name','Nome breve location');
            $crud->display_as('city','Città');
            $crud->display_as('address','Indirizzo');
            $crud->display_as('description','Testo descrizione');
            $crud->display_as('description_2','Orario e contatti');
            $crud->display_as('description_3','Altre informazioni');
            
            $crud->columns('store_short_name', 'store_name', 'city', 'address');
            $crud->fields('store_short_name', 'store_name', 'city', 'address', 'description', 'description_2', 'description_3');
            
            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            

//            $this->_list_output($output);

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function update_this_language($post_array, $primary_key) {
        
        /* Recupero la lingua dall'indirizzo */
        $last = $this->uri->total_segments();
        $lang = $this->uri->segment($last-2);
        
        try {
            /* Controllo se la lingua è già esistente nel db */
            $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));
            
            /* Se c'è già devo solo aggiornarla */
            if($query->num_rows() > 0) {
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
            else {
                $data = array('id' => $primary_key, 'lang' => $lang);
                $this->db->insert($this->table_i18n, $data);
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
            }
            
        } catch (Exception $ex) {
            return $ex;
        }

    }
    
	
    function gallery() {
        $this->css = array("admin.css");
        
        $image_crud = new image_CRUD();

        $image_crud->set_primary_key_field('photo_id');
        $image_crud->set_url_field('image');

        $image_crud->set_table($this->module_galleries);

        $image_crud
                ->set_ordering_field('ord')
                ->set_image_path($this->config->item('photo_image'))
                ->set_relation_field('photogallery_id');
                //->set_slider_size(500, 500);


        $output = $image_crud->render();
     
        /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
        $data['output'] = $output->output;
        $this->output = $this->load->view('pages/gallery_list', $data , true);

        /* Tramite il render del template caricherò*/
        $this->render_crud_page();

    }
    
    function edit_photo($_lang) {
        $this->css = array("admin.css");
        
        /* Recupero l'id della foto dall'indirizzo */
        $last = $this->uri->total_segments();
        $photo_id = $this->uri->segment($last);
        
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->photos_i18n);
            $crud->set_subject('lingua '.$_lang);
            $crud->unset_back_to_list();
            $crud->set_primary_key(array('lang' => $_lang, 'photo_id' => $photo_id));
            
            $crud->field_type('lang', 'hidden', $_lang);
            $crud->field_type('photo_id', 'hidden', $photo_id);
            
            $crud->display_as('description','Descrizione');
            $crud->display_as('other_desc','Descrizione secondaria');
            
            $crud->callback_update(array($this,'update_caption'));

            $crud->edit_fields('description', 'other_desc', 'lang', 'photo_id');

            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/edit_single_photo', $data , true);

            /* Tramite il render del template caricherò*/
            $this->render_crud_page();

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    /*
     * Funzione che sostituisce la update automatica di GroceryCRUD.
     * Occorre per poter gestire le didascalie nelle diverse lingue.
     */
    
    function update_caption($post_array, $primary_key) {
        /* Controllo se la didascalia è già stata inserita */
        $query = $this->db->get_where($this->photos_i18n, array('lang' => $post_array['lang'], 'photo_id' => $post_array['photo_id']));
        
        if($query->num_rows() > 0) {
            /* Se la didascalia esiste già la devo soltanto aggiornare */
            $this->db->where(array('lang' => $post_array['lang'], 'photo_id' => $post_array['photo_id']));
            $this->db->update($this->photos_i18n, $post_array);
        }
        else {
            /* Altrimenti devo inserire una nuova didascalia */
            $this->db->insert($this->photos_i18n, $post_array);
        }
    }
    
}

?>
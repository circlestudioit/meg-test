$(document).ready(function() {
    $(window).load(function() { // makes sure the whole site is loaded
        setTimeout(function(){
            $("#status").fadeOut(); // will first fade out the loading animation
            $("#preloader").delay(250).fadeOut("slow", function() {
                
                $('[data-submenu]').submenupicker();
                
            }); // will fade out the white DIV that covers the website.
        }, 500);
    });
    
    setupSquaresHp();
    setYTVideoSize();
    
    $('.videopreview').height($('.videopreview').width());
    
    if(document.getElementById('events-calendar')) {
        $.ajax({
            url: '/calendar_feed',
            type: 'POST',
            dataType: 'json',
            success: function(data){              
                $('#events-calendar').fullCalendar({
                    events: [data]
                });
            },
            error: function(e){
                console.log(e.responseText);

            }
        });
    }
    
    $("a.ajax-link").click(function(e) {
        //prevent default action
        e.preventDefault();

        //define the target and get content then load it to container
        var l = $(this).attr("href");
        link = l.split("/");
        baselink = link[0];
        pagelink = link[1];
        //console.log(baselink+'/'+pagelink);
        openSubPage(baselink, pagelink);
    });

    
    $('.openmenu').click(function() {
        if($('#menu-icon').hasClass('on')){
            $('#menu-icon').removeClass('on');
            $('nav#nav-home-mobile').fadeOut();
            $('ul.mobileuldrop').slideUp();
            $('body, html').css('overflow','visible');
        }
        else{
            $('nav#nav-home-mobile').fadeIn();
            $('#menu-icon').addClass('on');           
            $('body, html').css('overflow','hidden');
        }
    });
    
    $("a.mobiledrop").tap(function(e) {
        e.preventDefault();
        if($(this).hasClass('dropon')) {
  //          console.log('on');
            $(this).parent().children('ul.mobileuldrop').slideUp();
//            $(this).siblings('.dropon').children('ul.mobileuldrop').slideUp();
            $(this).removeClass('dropon');
        }
        else {
    //        console.log('off');
            $(this).addClass('dropon');
            $(this).parent().children("ul.mobileuldrop").slideDown();
        }

        
        //$(this).children("ul.mobileuldrop").not($(this)).first().fadeOut();
    });
    
    
//    $('a[href^="#"]').click(function(e) {
//        // Prevent the jump and the #hash from appearing on the address bar
//        e.preventDefault();
//        // Scroll the window, stop any previous animation, stop on user manual scroll
//        // Check https://github.com/flesler/jquery.scrollTo for more customizability
//        $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true});
//    });
    
    $('#submit_btn').click(function() {
        
        $('#form-messages').fadeIn();
        
        var form_data = {
            nome: $('#name').val(),
            azienda: $('#company').val(),
            email: $('#email').val(),
            message: $('#message').val()
        };

        var myForm = document.getElementById('main-contact-form');
        formData = new FormData(myForm);

        $.ajax({
            url: "/contattaci",
            dataType: 'text', 
            cache: false,
            contentType: false,
            processData: false,
            data: formData,                         
            type: 'post',
            success: function(msg) {
                $('#form-messages').html(msg);
                 setTimeout(function() {
                    $('#form-messages').fadeOut().html();
                    document.getElementById("main-contact-form").reset();
                }, 3000);
            }
        });
        return false;
    });


    

    
/* back to top button 
     // Nascondo l'icona al caricamento della pagina
    $("#back_to_top").hide();

    // Intercetto lo scroll di pagina
    $(window).scroll(function()
    {
        // Se l'evento scroll si verifica, mostro l'icona (invisibile) con effetto dissolvenza
        if ($("#back_to_top").is(":hidden")) {
            $("#back_to_top").fadeIn(500);
        }

        // Se si verifica il ritorno ad inizio pagina, nascondo l'icona con effetto dissolvenza
         if ($("body").scrollTop() == 0 && !$("#back_to_top").is(":hidden"))
         {
             $("#back_to_top").fadeOut(500);
         }
    });

    // Al click sull'icona, torno ad inizio pagina con movenza fluida
    $("#back_to_top").click(function()
    {
        $("html,body").animate({scrollTop: 0}, 500, function(){});
    });
*/
/* back to top button */
/*
$('.toc').click(function(){
      $.scrollTo( this.hash, 1500, { easing:'swing' });
      return false;
    });

*/
    
    

    
}); //document.ready
 
$(window).resize(function() {
    
    setupSquaresHp();
    
});

$(window).load(function() {
    $('.flexslider').flexslider();
});

$(window).bind("scroll", function(e) {

    if($(window).scrollTop() > 0) {

    }
    else {
        
    }
    
    /* START PARALLAX */
    if ($(window).scrollTop() < window.innerHeight)
    {
            var percent = Math.min(1, $(window).scrollTop() / window.innerHeight);
            //$(".home-title").css("top", (50+percent * 5)+"%");
//            $(".page-cover").css("top", (percent * -5)+"%");
//            $(".page-cover-top").css("top", (percent * -15)+"%");
//            $(".page-image-top").css("top", (percent * -15)+"%");
    }
    /* /START PARALLAX */

});
$(document).ready(function() {
	
    /*** Global Animations ***/

    var e = $(".sfl");
    e.each(function() {
            var e = $(this);
            e.hasClass("right") ? e.velocity({
                    opacity: 0,
                    translateY: "0px",
                    translateX: "150px"
            }) : e.velocity({
                    opacity: 0,
                    translateY: "0px",
                    translateX: "-150px"
            })
    }),$(window).on("scroll", function() {
            var e = $(".sfl");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.hasClass("right") ? t.velocity({
                                    opacity: 1,
                                    translateY: "0px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            }) : t.velocity({
                                    opacity: 1,
                                    translateY: "0",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            })
                    }
            });
    });

    var e = $(".bottomtop");
    e.each(function() {
        e.velocity({
                opacity: 0,
                translateY: "50px",
                translateX: "0px"
        })
    }),$(window).on("scroll", function() {
            var e = $(".bottomtop");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 1,
                                    translateY: "0px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            })
                    }
            });
    });
    
    var e = $(".bottomtop-d1");
    e.each(function() {
        e.velocity({
                opacity: 0,
                translateY: "50px",
                translateX: "0px"
        })
    }),$(window).on("scroll", function() {
            var e = $(".bottomtop-d1");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 1,
                                    translateY: "0px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            })
                    }
            });
    });
    
    var e = $(".bottomtop-d2");
    e.each(function() {
        e.velocity({
                opacity: 0,
                translateY: "50px",
                translateX: "0px"
        })
    }),$(window).on("scroll", function() {
            var e = $(".bottomtop-d2");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 1,
                                    translateY: "0px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 700
                            })
                    }
            });
    });
    
    var e = $(".bottomtop-d3");
    e.each(function() {
        e.velocity({
                opacity: 0,
                translateY: "50px",
                translateX: "0px"
        })
    }),$(window).on("scroll", function() {
            var e = $(".bottomtop-d3");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 1,
                                    translateY: "0px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 900
                            })
                    }
            });
    });

    var e = $(".fade");
    e.each(function() {
            var e = $(this);
            e.velocity({
                    opacity: 0,
            })
    }),$(window).on("scroll", function() {
            var e = $(".fade"),
                    i = 0;

            e.each(function(e, n) {
                    var n = $(n);
                    i++;
                    if (n.visible(!0)) {
                            var t = $(this);
                             t.velocity({
                                    opacity: 1,
                            }, {
                                     delay: i * 250
                            })
                    }
            });
    });


    var e = $(".fadeOut");
    e.each(function() {
        e.velocity({
                opacity: 1,
                translateY: "0px",
                translateX: "0px"
        })
    }),$(window).on("scroll", function() {
            var e = $(".fadeOut");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 0,
                                    translateY: "50px",
                                    translateX: "0px"
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            })
                    }               
            })
        });

    var e = $(".fadeIn");
    e.each(function() {
        e.velocity({
                opacity: 0,
        })
    }),$(window).on("scroll", function() {
            var e = $(".fadeIn");
            e.each(function(e, n) {
                    var n = $(n);
                    if (n.visible(!0)) {
                            var t = $(this);
                            t.velocity({
                                    opacity: 1,
                            }, {
                                    easing: [.2, .62, .37, .96],
                                     delay: 500
                            })
                    }               
            })
        });
        
        var e = $(".bottomTopOpen");
        e.each(function() {
            e.velocity({
                    opacity: 0,
                    translateY: "50px",
                    translateX: "0px"
            })
        }),$(window).load(function() {
		var e = $(".bottomTopOpen");
		e.each(function(e, n) {
			var n = $(n);
			if (n.visible(!0)) {
				var t = $(this);
				t.velocity({
					opacity: 1,
					translateY: "0px",
					translateX: "0px"
				}, {
					easing: [.2, .62, .37, .96],
					 delay: 700
				})
			}
		});
	});
        
        var e = $(".bottomTopOpenDelay");
        e.each(function() {
            e.velocity({
                    opacity: 0,
                    translateY: "50px",
                    translateX: "0px"
            })
        }),$(window).load(function() {
		var e = $(".bottomTopOpenDelay");
		e.each(function(e, n) {
			var n = $(n);
			if (n.visible(!0)) {
				var t = $(this);
				t.velocity({
					opacity: 1,
					translateY: "0px",
					translateX: "0px"
				}, {
					easing: [.2, .62, .37, .96],
					 delay: 1000
				})
			}
		});
	});
        
        var e = $(".topBottomOpen");
        e.each(function() {
            e.velocity({
                    opacity: 0,
                    translateY: "-50px",
                    translateX: "0px"
            })
        }),$(window).load(function() {
		var e = $(".topBottomOpen");
		e.each(function(e, n) {
			var n = $(n);
			if (n.visible(!0)) {
				var t = $(this);
				t.velocity({
					opacity: 1,
					translateY: "0px",
					translateX: "0px"
				}, {
					easing: [.2, .62, .37, .96],
					 delay: 500
				})
			}
		});
	});
			

//    var t = $('a.white');
//    t.on("mouseenter", function() {
//        $(this).velocity({
//            textDecoration: "underline"
//        }, {
//            duration: 500,
//            easing: "linear"
//        })
//    }), t.on("mouseleave", function() {
//            $(this).velocity({
//                textDecoration: "none"
//        }, {
//                duration: 400,
//                easing: "linear"
//        });
//    });
	
	/*** Home Animations ***/
	
/*	var t = $('.piacere-paziente');
	t.on("mouseenter", function() {
		$(this).velocity({
				color: "#000",
				backgroundColor: "#fff"
			}, {
				duration: 500,
				easing: "linear"
		})
     }), t.on("mouseleave", function() {
			$(this).velocity({
					color: "#fff",
					backgroundColor: "#000"
				}, {
					duration: 400,
					easing: "linear"
				})
      });*/
	  
	  /*** Gianni Masciarelli Animations ***/
//	  var t = $('#gianni-quotes'),
//	  	  n = $('.bio-white-wrapper');
//		  n.velocity({
//				opacity: 0,
//				translateY: "150px",
//				translateX: "0px"
//			});

});
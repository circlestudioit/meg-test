<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
        
        
        public function index() {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */
            $detect = new Mobile_Detect();
            
            $this->title = "MEG | Mercato Enogastronomico ";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            
            $this->css = array("main.css", "responsive.css", "flexslider.css", "hoverset1.css"); 
            $this->fonts = array("Libre+Baskerville:400,400i","Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));

            
            if ($detect->isMobile())
            {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js",
                                      "libs/jquery.flexslider-min.js", "libs/jquery.scrollTo.min.js",  "libs/skel.min.js", "functions.js", "script.js", "libs/theme/util.js", "libs/theme/main.js", "libs/theme/numscroller.js");
            }
            else {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js",
                                      "libs/jquery.flexslider-min.js",  "libs/jquery.scrollTo.min.js",  "libs/skel.min.js", "functions.js", "script.js", "libs/theme/util.js", "libs/theme/main.js", "libs/theme/numscroller.js");

            }
            
            $this->_render('home');

        }
        
        public function send_mail() {
            
            $this->load->library('email');
            $this->load->library('form_validation');
            //$this->load->library('upload');
            $this->load->helper(array('form', 'url'));
            
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('azienda', 'Azienda', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('message', 'Messaggio', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                $errors = validation_errors();
//                echo json_encode($errors);
                $curpage = str_replace("/index.php", "", $this->session->userdata('currentPage'));
                //$this->session->set_flashdata('errors', $errors);
                return print_r($errors);
                /* L'errore è gestito via ajax in script.min.js */
                
                //redirect($curpage);
            }
            else {
                $config['charset']          = 'utf-8';
                $config['mailtype']         = 'html';
                $this->email->initialize($config);

                $this->email->from($_POST['email'], $_POST['nome']);
                $this->email->to('info@megmarket.it'); 
                //$this->email->to('web@circlestudio.it'); 
                $this->email->bcc('web@circlestudio.it');

                $this->email->subject('MEG Mercato enogastronomico Website Contact');
                $this->email->message(
                        "<!DOCTYPE html>
                        <head>
                        <meta charset=\"utf-8\">
                        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
                        <title>Consorzio di Tutela</title>
                        </head>
                        <body>
                        Nome: ".(isset($_POST['nome'])? $_POST['nome'] : "")."<br />".
                        "Email: ".(isset($_POST['azienda'])? $_POST['azienda'] : "")."<br />".
                        "Email: ".(isset($_POST['email'])? $_POST['email'] : "")."<br /><br />".
                        "Messaggio: ".$_POST['message']."<br /><br />".
                        "</body></html>"

                );	


                echo $this->email->print_debugger();

                $this->email->send();
                //$curpage = str_replace("/index.php", "", $this->session->userdata('currentPage'));
                //redirect($curpage);
                return print_r($this->lang->line('grazie-per-aver-inviato'));
            }
            
        }
               
}
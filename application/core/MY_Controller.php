<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
    //Page info
    protected $data = Array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    //Page contents
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;
    protected $detect = '';
	
    function __construct()
    {

            parent::__construct();
//            $this->data["uri_segment_1"] = $this->uri->segment(1);
//            $this->data["uri_segment_2"] = $this->uri->segment(2);
            $this->title = $this->config->item('site_title');
            $this->description = $this->config->item('site_description');
            $this->keywords = $this->config->item('site_keywords');
            $this->author = $this->config->item('site_author');
            
            $this->session->set_userdata('currentPage', current_url());

            $supportedLangs = $this->config->item('active_langs');
            $check_language = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : $this->config->item('l');  
            $languages = explode(',',substr($check_language, 0, 2));
                
            if(!$this->session->userdata('userlang')) {
                foreach($languages as $lang)
                {
                    if(in_array($lang, $supportedLangs))
                    {
                    // Set the page locale to the first supported language found
                    $this->session->set_userdata('lang', $lang);
                    setlocale(LC_ALL, $lang.'_'.strtoupper($lang));
                    break;

                    }
                    else {
                        //Imposto la lingua di default
                        $this->session->set_userdata('lang', $this->config->item('language'));
                        setlocale(LC_ALL, $this->config->item('language'));
                    }
                }
            }

            $this->pageName = strToLower(get_class($this));

            $this->load->library('form_validation');
            $this->load->library('Mobile_Detect');
            
            $this->lang->load('main', $this->session->userdata('lang'));
            $this->lang->load('menu', $this->session->userdata('lang'));
            $this->lang->load('error', $this->session->userdata('lang'));

            $this->load->model('page_model');
            $this->load->model('gallery_model');
            $this->load->model('post_model');
            $this->load->model('projects_model');
            $this->load->model('menu_model');

            // load model che vanno caricati nel content type
            /*
            Creare un modello "globale" che legge la tabella page_contents e fa il load automatico dei modelli
             */             
            
            $this->load->model('pages_content_model');
            $all_page_content_model = $this->pages_content_model->get_all_page_content();
            
            foreach($all_page_content_model as $name){
                $var = $name['content_name'].'_model';
                $this->load->model($var);
            }

            $this->load->helper('text');
            
            $this->detect = new Mobile_Detect();
            
    }
        
        
    /****************************
     * 
     *           HOME
     * 
     ***************************/
        
        
    protected function _render($view) {
        
        $this->session->set_userdata('currentPage', current_url());
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        //meta
        
        $toTpl["author"] = $this->author;
        
        
        $this->template = "main";        
        
        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
        $toTpl["isMobile"] = $this->detect->isMobile();
        
        if(empty($toTpl["pagecontent"])) {
            $error['error_message'] = $this->lang->line('page-not-found');
            $toBody["content_body"] = $this->load->view('template/error', $error,true);
        }
        
        else {
            
            $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
            $toTpl["title"] = $this->title;
            
            $res = $this->getPageChildren($view, $toTpl);
            $toTpl['pageChildren'] = $res['children'];
            //$toTpl['submenu'] = $res['submenu'];

           //print_r($toTpl["pagecontent"]);
            $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
            $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

            /* Carico la galleria associata alla pagina */            
            $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
            /* Carico la galleria associata alla pagina */            
            $toTpl['gallery_2'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_2_id']);
            
            /* Featured Projects */
            $toTpl['projects'] = $this->projects_model->get_featured_projects();
            
            /* News per Footer */
            //$toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);
            $toTpl['highlight_post'] = $this->post_model->get_highlight_post();
            $events = $this->post_model->get_posts_for_category("eventi");
            $toTpl['next_event']    = $events[0];
            /* Featured gallery */
            //$toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
            $feat = $this->gallery_model->get_featured_random_photo();
            $toTpl['pagecontent']['featph'] = $feat[0];
            //print_r($toTpl['pagecontent']['featph']);

            /* Tipologia post associati a questa pagina */
            $toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

            //data
            $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
        }
        
        $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);
        
        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');

        //$toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        $toNav['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        //$toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    /****************************
     * 
     *      PAGINE INTERNE
     * 
     ***************************/
    
    public function _render_page() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        /*Struttura principale: header - body - footer */
        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        if(empty($toTpl["pagecontent"])) {
            show_404();
        }
        
        /* Imposto il template della pagina */
        $template = $toTpl["pagecontent"]['template_name'];
        
        $toTpl["isMobile"] = $this->detect->isMobile();
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $this->title." - ".$toTpl["pagecontent"]['page_title'];
    
        /* Carico il contenuto associato alla pagina */
        /*
            $toTpl["pagecontent"]['content_name'] deve essere uguale al nome del modello da caricare
        */
        $toTpl['contenuto'] = null;
        if($toTpl["pagecontent"]['content_name'] != 'default') {
            $model = $toTpl["pagecontent"]['content_name']."_model"; //  prendo il modello corrispondente
            $toTpl['contenuto'] = $this->$model->get_full_content(); // carico tutto il contenuto della tabella legata al modello selezionato
            /*
             * Sviluppare un sistema con cui non solo scelgo il tipo di contenuto da mostrare, ma scelgo anche COME mostrarlo, ovvero posso scegliere 
             * (tramite una seconda dropdown a fianco a content) quale query (funzione _my_function()) andrà chiamata nel modello.
             * Devo in sostanza estendere l'attuale tabella page_contents con una tabella ad essa relazionata che abbia la lista di tutte le funzioni disponibili nel corrispondente modello.
             * $toTpl['come_mostrare_il_contenuto'] = $this->$model->_my_function() //es. get_all / get_una / get_asc / get_desc .... ecc...
             *
             */
        }
       
           
        /* MENU associato alla Pagina */
        if($toTpl["pagecontent"]['menu_id']!= NULL) {
            $toTpl['navigation_parent'] = $this->menu_model->get_by_id($toTpl["pagecontent"]['menu_id']);
            $toTpl['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        }

        //data
        $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);
 

        // $toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        
        

        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');
        
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);
    }
    
    
    
   /******************************
    * 
    *    TEMPLATE NEWS & PRESS
    * 
    ******************************/
    
    protected function _render_template_news() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        $toTpl["isMobile"] = $this->detect->isMobile();
        
        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        /* Imposto il template della pagina */
        $template = 'pages/news';
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['page_title'];
        
        /* Tipologia post associati a questa pagina */
        $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('degustazioni');

        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        /* SOTTO-MENU PAGINA */
        $res = $this->getPageChildren($page_name, $toTpl);
        $toTpl['children'] = $res['children'];
        $toTpl['submenu'] = $res['submenu'];

        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    /******************************
    * 
    *      ESPLOSO NEWS & PRESS
    * 
    ******************************/
    
    protected function _render_news($post = '') {
        
        $last = $this->uri->total_segments();
        $page_url = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";
        
        $toTpl["isMobile"] = $this->detect->isMobile();

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->post_model->get_post_by_url($page_url);

        /* Imposto il template della pagina */
        $template = 'dettaglio-news';
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['title']; 
        
        /* Carico la galleria associata alla news */            
        $toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        /* Carico l'evento associato alla news   */            
        $toTpl["pagecontent"]['evento'] = $this->post_model->get_by_id($toTpl["pagecontent"]['related_post_id']);

        /* MENU associato alla Pagina */
        if($toTpl["pagecontent"]['menu_id']!= NULL) {
            $toTpl['navigation_parent'] = $this->menu_model->get_by_id($toTpl["pagecontent"]['menu_id']);
            $toTpl['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        }

        //data
        $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);
 

        // $toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        
        

        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');
        
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);

        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    /******************************
    * 
    *      ESPLOSO EVENTO
    * 
    ******************************/
    
    protected function _render_evento($post = '') {
        
        $last = $this->uri->total_segments();
        $page_url = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;
        
        $toTpl["isMobile"] = $this->detect->isMobile();

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->post_model->get_post_by_url($page_url);

        /* Imposto il template della pagina */
        $template = 'dettaglio-evento';
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['title']; 
        
        /* Carico la galleria associata all'evento */            
        $toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        /* Carico le news associato all'evento   */            
        $toTpl["pagecontent"]['news'] = $this->post_model->get_by_id($toTpl["pagecontent"]['related_post_id']);

        /* MENU associato alla Pagina */
        if($toTpl["pagecontent"]['menu_id']!= NULL) {
            $toTpl['navigation_parent'] = $this->menu_model->get_by_id($toTpl["pagecontent"]['menu_id']);
            $toTpl['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        }

        //data
        $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);
 

        // $toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        
        

        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');
        
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);

        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    /******************************
    * 
    *      ESPLOSO TESTIMONIANZA
    * 
    ******************************/
    
    protected function _render_testimonianza($post = '') {
        
        $last = $this->uri->total_segments();
        $page_url = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;
        
        $toTpl["isMobile"] = $this->detect->isMobile();

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->testimonianze_model->get_post_by_url($page_url);

        /* Imposto il template della pagina */
        $template = 'dettaglio-testimonianza';
        $toTpl["pagecontent"]['menu_id'] = 23;
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['title']; 
        
        /* Carico la galleria associata all'evento */            
        $toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        /* Carico le news associato all'evento   */            
        $toTpl["pagecontent"]['news'] = $this->post_model->get_by_id($toTpl["pagecontent"]['related_post_id']);

        /* MENU associato alla Pagina */
        if($toTpl["pagecontent"]['menu_id']!= NULL) {
            $toTpl['navigation_parent'] = $this->menu_model->get_by_id($toTpl["pagecontent"]['menu_id']);
            $toTpl['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        }

        //data
        $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);
 

        // $toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        
        

        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');
        
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);

        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    /****************************
     * 
     *      PAGINA CALENDARIO
     * 
     ***************************/
    
    public function _render_calendar() {
        
        $toTpl['contenuto'] = $this->post_model->get_posts_for_category('eventi');


        //render view
        $this->load->view("pages/calendar",$toTpl);
    }
    
    
    /******************************
    * 
    *      ESPLOSO GALLERY
    * 
    ******************************/
    
    protected function _render_gallery($gallery = '') {
        
        $last = $this->uri->total_segments();
        $page_url = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $toTpl["isMobile"] = $this->detect->isMobile();
        
        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $gal = $this->gallery_model->get_gallery_by_name($gallery);
        
        $toTpl["pagecontent"]["gallery_info"] = $this->gallery_model->get_gallery_info($gal['photogallery_id']);
        $toTpl["pagecontent"]["gallery_photos"] = $this->gallery_model->get_photos_by_gallery_id($gal['photogallery_id']);
        

        /* Imposto il template della pagina */
        $template = 'photogallery';
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["gallery_info"]['title']; 
        
        /* MENU associato alla Pagina */
        if($toTpl["pagecontent"]['menu_id']!= NULL) {
            $toTpl['navigation_parent'] = $this->menu_model->get_by_id($toTpl["pagecontent"]['menu_id']);
            $toTpl['navigation'] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        }

        //data
        $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);
 

        // $toNav['main_nav'] = $this->menu_model->get_menu_items(0); /* _MAIN_MENU */
        $toNav['main_nav'] = $this->menu_model->get_full_menu(0); /* _MAIN_MENU */
        
        $toNav['secondary_nav'] = $this->menu_model->get_menu_items(33); /* _SECONDARY_MENU */
        
        

        $toFooter["footer_content"] = $this->page_model->get_page_full_content('footer');
        if($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-mobile",$toNav,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",$toNav,true);  
        }
        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",$toFooter,true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);

        //render view
        $this->load->view("template/skeleton",$toTpl);

    }

     
    public function getPageChildren($page_name, $toTpl) {
        $submenu = array();

        /* RECUPERO TUTTI I FIGLI DELLA PAGINA */
        $page_id = $this->page_model->get_id_by_name($page_name);
        $children_array = $this->page_model->get_page_children_of($page_id);
        $children = array();

        /* Verifico se la pagina principale va inserita nel submenu */
        if($toTpl["pagecontent"]['on_menu']) {
            array_push($submenu, $toTpl["pagecontent"]);
        }

        /* Verifico tutti i figli della pagina */
        foreach($children_array as $child) {
            $childcontent = $this->page_model->get_page_full_content($child['name']);
            if(isset($childcontent['photogallery_id'])) {
                $childcontent['gallery'] = $this->gallery_model->get_photos_by_gallery_id($childcontent['photogallery_id']);
            }
            /* prendo l'url della pagina */
            $childcontent['menu'] = $this->menu_model->get_by_id($childcontent['menu_id']);
            
            array_push($children, $childcontent);

            /* Verifico se la pagina figlia va inserita nel menu */
            if($child['on_menu'] == 1) {
                array_push($submenu, $childcontent);
            }
        }
        return array('children' => $children, 'submenu' => $submenu);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Video_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'videos';  // modify here
    var $table_i18n = 'videos_i18n'; // modify here

    public function get_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }

    public function get($per_page, $offset, $sort_by) {

        $query = $this->db->query("
          SELECT ".$this->table.".id, ".$this->table.".date,".
          $this->table.".name, ".$this->table.".embed, ".$this->table.".visible
           FROM ".$this->table.
          " ORDER BY ".$this->table.".".$sort_by." ASC".
          " LIMIT ".$offset.", ".$per_page);
        return $query->result_array();
    }

}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table                              = 'menu';  // Tabella madre
    var $table_i18n                         = 'menu_i18n';  // Tabella lingue


    public function get_by_id($menu_id) {
        $this->db->join($this->table_i18n, $this->table_i18n.'.id = '.$this->table.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.id' => $menu_id), 1,0);
        return $query->row_array();
    }
    
    public function get_menu_items($menu_id) {
        $this->db->order_by('ord', 'ASC');
        $this->db->join($this->table_i18n, $this->table_i18n.'.id = '.$this->table.'.id');
        $query = $this->db->get_where($this->table, array('parent_id' => $menu_id, 'published' => 1, 'lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
   
    public function get_full_menu($menu_id) {
        $results = $this->get_menu_items($menu_id);
        $result = null;
        $i = 0;
        foreach ($results as $res) {
            $result[$i] = $res;
            if($res['url'] != '/'){
                $sub_menu_parent_id = $this->get_by_url($res['url']);
                $sub_menu = $this->get_menu_items($sub_menu_parent_id['id']);
                $result[$i]['sub_menu'] = $sub_menu;
            }else{
                $result[$i]['sub_menu'] = NULL;
            }
            
            $i++;
        }

        return $result;
    }
    
    public function get_by_name($menu_name) {
        $this->db->join($this->table_i18n, $this->table_i18n.'.id = '.$this->table.'.id');
        $query = $this->db->get_where($this->table, array('name' => $menu_name, $this->table_i18n.'.lang' => $this->session->userdata('lang')), 1, 0);
        return $query->row_array();
    }

    public function get_by_url($menu_url) {
        $this->db->join($this->table_i18n, $this->table_i18n.'.id = '.$this->table.'.id');
        $query = $this->db->get_where($this->table, array('url' => $menu_url,'is_item' => 0, $this->table_i18n.'.lang' => $this->session->userdata('lang')), 1, 0);
        return $query->row_array();
    }

}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages_content_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'pages_content';  // modify here

    
    public function get_by_id($id) {
        $this->db->where($this->table.'.content_name != "default"');
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_id_by_name($name) {
        $this->db->where($this->table.'.content_name != "default"');
        $query = $this->db->get_where($this->table, array('name' => $name), 1, 0);
        return $query->row()->id;
    }
    
    public function get_name_by_id($id) {
        $this->db->where($this->table.'.content_name != "default"');
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row()->name;
    }

    public function get_all_page_content(){
        $this->db->where($this->table.'.content_name != "default"');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function count() {
        $this->db->where($this->table.'.content_name != "default"');
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }



    
}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gallery_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'photogalleries';  // modify here
    var $table_i18n = 'photogalleries_i18n'; // modify here
   
    var $photos_table = 'photos';
    var $photos_i18n_table = 'photos_i18n';

   
    public function get_gallery_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }

    public function get_gallery_by_name($name) {
        $query = $this->db->get_where($this->table, array('name' => $name), 1, 0);
        $result = $query->row_array();
        if(!empty($result)) 
            return $result;
        else
            return array();
    }
    
    public function get_featured_gallery() {
        $this->db->join($this->table_i18n, $this->table.'.photogallery_id = '.$this->table_i18n.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.featured' => TRUE, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        return $result[0];
    }
    
    public function get_featured_random_photo() {
        $this->db->where($this->table.'.featured', TRUE);
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        $featured_gallery = $this->get_random_by_gallery_id($result[0]['photogallery_id']);
        return $featured_gallery;
    }
    
    public function get_featured_gallery_photos() {
        $this->db->where($this->table.'.featured', TRUE);
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        $featured_gallery = $this->get_photos_by_gallery_id($result[0]['photogallery_id']);
        return $featured_gallery;
    }
    
    public function get_gallery_info($photogallery_id) {
        $this->db->join($this->table_i18n, $this->table.'.photogallery_id = '.$this->table_i18n.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.photogallery_id' => $photogallery_id, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        if(!empty($result))
            return $result[0];
        else
            return array();
    }

    public function get_photos_by_gallery_id($photogallery_id) {
        $this->db->join($this->photos_i18n_table, $this->photos_i18n_table.'.photo_id = '.$this->photos_table.'.photo_id');
        $this->db->where($this->photos_i18n_table.'.lang', $this->session->userdata('lang'));
        $this->db->order_by($this->photos_table.'.ord','ASC');
        $query = $this->db->get_where($this->photos_table, array('photogallery_id' => $photogallery_id));
        return $query->result_array();
    }
    
    public function get_random_by_gallery_id($photogallery_id) {
        $this->db->join($this->photos_i18n_table, $this->photos_i18n_table.'.photo_id = '.$this->photos_table.'.photo_id');
        $this->db->where($this->photos_i18n_table.'.lang', $this->session->userdata('lang'));
        $this->db->order_by('RAND()');
        $query = $this->db->get_where($this->photos_table, array('photogallery_id' => $photogallery_id), 1,0);
        return $query->result_array();
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }



    
}

?>
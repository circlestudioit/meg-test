<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Galleryevents_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'photogalleries';  // modify here
    var $table_i18n = 'photogalleries_i18n'; // modify here
   
    var $photos_table = 'photos';
    var $photos_i18n_table = 'photos_i18n';

    
    public function get_full_content() {
        $galleries = $this->get_photogalleries();
        $i = 0;
        $result = array();
        foreach($galleries as $gallery) {
            $result[$i]['info'] = $this->get_gallery_info($gallery['photogallery_id']);
            $result[$i]['photos'] = $this->get_photos_by_gallery_id($gallery['photogallery_id']);
            $i++;
            
        }
        return $result;
    }
   
    public function get_by_id($id) {
       /* $query = $this->db->query("
        SELECT
        ");*/
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_featured_gallery() {
        $this->db->join($this->table_i18n, $this->table.'.photogallery_id = '.$this->table_i18n.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.featured' => TRUE, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        return $result[0];
    }
    
    public function get_featured_gallery_photos() {
        $this->db->where($this->table.'.featured', TRUE);
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        $featured_gallery = $this->get_photos_by_gallery_id($result[0]['photogallery_id']);
        return $featured_gallery;
    }
    
    public function get_gallery_info($photogallery_id) {
        $this->db->join($this->table_i18n, $this->table.'.photogallery_id = '.$this->table_i18n.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.photogallery_id' => $photogallery_id, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        if(!empty($result))
            return $result[0];
        else
            return array();
    }
    
    public function get_photogalleries() {
        //$this->db->join($this->table_i18n, $this->table.'.photogallery_id = '.$this->table_i18n.'.id');
        $query = $this->db->get_where($this->table, array($this->table.'.published' => TRUE));
        $result = $query->result_array();
        if(!empty($result))
            return $result;
        else
            return array();
    }

    public function get_photos_by_gallery_id($photogallery_id) {
        $this->db->join($this->photos_i18n_table, $this->photos_i18n_table.'.photo_id = '.$this->photos_table.'.photo_id');
        $this->db->where($this->photos_i18n_table.'.lang', $this->session->userdata('lang'));
        $this->db->order_by($this->photos_table.'.ord','ASC');
        $query = $this->db->get_where($this->photos_table, array('photogallery_id' => $photogallery_id));
        return $query->result_array();
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }

    public function get($per_page, $offset, $sort_by) {

        $query = $this->db->query("
          SELECT ".$this->table.".id, ".$this->table.".date,".
          $this->table.".published, ".$this->table.".name
          FROM ".$this->table.
          //" LEFT JOIN ".$this->table_i18n." ON ".$this->table_i18n.".id = ".$this->table.".id".
          //" WHERE ".$this->table_i18n.".lang = '".$this->session->userdata['language']."'".
          " ORDER BY ".$this->table.".".$sort_by." ASC".
          " LIMIT ".$offset.", ".$per_page);
        return $query->result_array();
    }

   
    
    public function get_content($name) {

        $query = $this->db->query("
          SELECT ".$this->table.".*, ".$this->table_i18n.".*
          FROM ".$this->table." 
          LEFT JOIN ".$this->table_i18n." ON ".$this->table_i18n.".id = ".$this->table.".id
          WHERE ".$this->table.".name = '".$name."'
          AND ".$this->table.".public = 1 
          AND ".$this->table_i18n.".lang = '".$this->session->userdata('lang')."'"
          );
        
        if($query->row() != null) {
            return $query->row();
        }
        else return array();
    }



    
}

?>
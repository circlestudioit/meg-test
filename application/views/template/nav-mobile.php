<!-- Menu -->
<nav id="menu">
    <a href="#slider-hp">
        <img src="<?= base_url(IMAGES."logo-white.png")?>" class="img-responsive logo-menu" />
    </a>
    <ul>
        <li><span class="meg">01</span> <a href="#meg">Cos'è meg</a></li>
        <li><span class="progetto">02</span> <a href="#progetto">Come realizziamo il progetto</a></li>
        <li><span class="offerta">03</span> <a href="#offerta">Cosa Offriamo</a></li>
        <li><span class="buyers">04</span> <a href="#buyers">Buyers</a></li>
        <li><span class="contatti">05</span> <a href="#contatti">Contatti</a></li>
    </ul>
</nav>

<!-- Menu -->
<nav id="menu">
    <a href="#slider-hp">
        <img src="<?= base_url(IMAGES."logo-white.png")?>" class="img-responsive logo-menu" />
    </a>
    <ul>
        <li><span class="meg">01</span> <a href="#meg">Cos'è meg</a></li>
        <li><span class="progetto">02</span> <a href="#progetto">Come realizziamo il progetto</a></li>
        <li><span class="offerta">03</span> <a href="#offerta">Cosa Offriamo</a></li>
        <li><span class="buyers">04</span> <a href="#buyers">Buyers</a></li>
        <li><span class="contatti">05</span> <a href="#contatti">Contatti</a></li>
    </ul>
</nav>

<?php /* 
<nav id="nav-home" class="navbar">
    <ul class="menu-items text-left">
        <?php
        /* Menu PRINCIPALE DEL SITO  
        foreach($main_nav as $menuItem): ?>
        <?php if($menuItem['sub_menu'] == NULL){ ?>
            <li>
                <a class="neutral link" href="<?=base_url($menuItem['url'])?>">
                <?=$menuItem['title']?></a>
            </li>
        <?php }else{ ?>    
            <li class="dropdown">
                <a class="neutral link" href="<?=base_url($menuItem['url'])?>">
                    <?=$menuItem['title']?>
                    <img src="<?=base_url(IMAGES."scroll-down-nav.png")?>" class="freccia-nav" />
                </a>
                <ul class="dropdown-menu">
                    <?php foreach ($menuItem['sub_menu'] as $sub_menu): ?>
                        <li class="dropdown-submenu">
                            <a class="ClanBook dark" href="<?= base_url($menuItem['url'].'/'.$sub_menu['url']); ?>">
                                <?=strtoupper($sub_menu['title']); ?>
                            </a>
                        </li>
                        <li class="brown-line-nav"></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php } ?>    
        <?php endforeach; ?>
    </ul>    
</nav>
*/ ?>
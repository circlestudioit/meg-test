<!-- Footer -->
    <footer class="main-footer">
        <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgcolor-light">
            <path d="M0 0 L50 100 L100 0 Z" fill="#fff" stroke="#fff"></path>
        </svg>
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-xl-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xl-offset-1 footer-widgets">
                    <!-- Start Contact Widget -->
                    <div class="col-md-9 col-xs-12">
                        <div class="footer-widget contact-widget">
                            <img src="<?= base_url(IMAGES."logo-white.png")?>" class="logo-footer img-responsive" alt="Footer Logo" />
                            <?php /*
                            <ul class="social-icons">
                                <li>
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                
                                <li>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="google" href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                </li>
                                <li>
                                    <a class="linkdin" href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a class="flickr" href="#"><i class="fa fa-flickr"></i></a>
                                </li>
                                <li>
                                    <a class="tumblr" href="#"><i class="fa fa-tumblr"></i></a>
                                </li>
                                <li>
                                    <a class="instgram" href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="vimeo" href="#"><i class="fa fa-vimeo-square"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href="#"><i class="fa fa-skype"></i></a>
                                </li>
                                
                            </ul>
                            */ ?>
                        </div>
                    </div><!-- .col-md-3 -->
                    <!-- End Contact Widget -->

                    <!-- Start Flickr Widget -->
                    <div class="col-md-3 col-xs-12">
                        <div class="footer-widget company-links">   
                            <ul class="footer-links">
                                <li><span class="meg">01</span> <a href="#meg" class="toc">Cos'è meg</a></li>
                                <li><span class="progetto">02</span> <a href="#progetto">Come</a></li>
                                <li><span class="offerta">03</span> <a href="#offerta">Cosa Offriamo</a></li>
                                <li><span class="buyers">04</span> <a href="#buyers">Buyers</a></li>
                                <li><span class="contatti">05</span> <a href="#contatti">Contatti</a></li>
                            </ul>
                        </div>
                    </div><!-- .col-md-3 -->
                    <!-- End Flickr Widget -->
                </div><!-- .row -->
                <div class="clearfix"></div>
                <!-- Start Copyright -->
                
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="copyright-section text-center">
                        <small>&copy; 2017 MEG Mercato Enogastronomico / DESIGN &amp; CODE: 
                        <a class="neutral" href="http://www.circlestudio.it" target="_blank">CIRCLESTUDIO</a></small>
                    </div>
                </div>
                <div class="clearfix"></div>
    </footer>
<?php /*
<div id="slider-hp" class="flexslider col-xs-12 col-sm-12">
    <ul class="slides" style="background-image: url(<?php base_url(IMAGES."vuota.jpg")?>); background-size: 100%;">
        <?php foreach($gallery as $pic): ?>
            <li class="slide" width="100%">
                <img src="<?= base_url($this->config->item('photo_image').$pic['image']); ?>" class="sliderimage"  width="1920" heigth="1080" />
            </li>
        <?php endforeach; ?>
    </ul>
</div> */ ?>
<div id="slider-hp" class="col-xs-12 col-sm-12">
  <img src="<?= base_url($this->config->item('pages_cover').$pagecontent['cover']); ?>" class="img-responsive center-block" /> 
</div> 
<div class="clear"></div>
<div id="" class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2"  style="padding: 6em 0;">
        <img src="<?= base_url($this->config->item('pages_thumb').$pagecontent['thumb']); ?>" class="img-responsive center-block" />
        <h2 class="text-center"><?=$pagecontent['title']?></h2>
    </div>
</div>

<div id="facts" class="facts">
    <div class="container">
        <div class="row text-center col-xs-offset-1 col-xs-10">
            <div class="col-xs-12 col-lg-3 col-md-3">
                <div class="count-item">
                    <i class="lnr lnr-clock"></i>
                    <div class="numscroller" data-slno='1' data-min='0' data-max='4444' data-delay='10' data-increment="10">9909</div>
                    <div class="count-name-intro">Hours</div>
                </div>
            </div>
            <div class="col-xs-12  col-lg-3 col-md-3">
                <div class="count-item">
                    <i class="lnr lnr-rocket"></i>
                    <div class="numscroller" data-slno='1' data-min='0' data-max='1111' data-delay='10' data-increment="5">1111</div>
                    <div class="count-name-intro">Projects</div>
                </div>
            </div>
            <div class="col-xs-12  col-lg-3 col-md-3">
                <div class="count-item">
                    <i class="lnr lnr-coffee-cup"></i>
                    <div class="numscroller" data-slno='1' data-min='0' data-max='1555' data-delay='10' data-increment="5">1555</div>
                    <div class="count-name-intro">coffe's</div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-3 col-md-3">
                <div class="count-item">
                    <i class="lnr lnr-camera"></i>
                    <div class="numscroller" data-slno='1' data-min='0' data-max='4444' data-delay='10' data-increment="10">4444</div>
                    <div class="count-name-intro">Photos taken</div>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="meg" class="team mbr-box mbr-section mbr-section--relative">
    <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L50 100 L100 0 Z" fill="#f5f6e3" stroke="#f5f6e3"></path>
    </svg>
    <div class="container">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers meg nexa-black"><?= $pageChildren[0]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[0]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[0]['content']; ?><br />
                    <?= $pageChildren[0]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
    </div>
</section>
<div class="col-md-offset-1 col-md-6 col-sm-8 col-sm-offset-1 ">
    <img src="<?= base_url($this->config->item('pages_thumb').$pageChildren[0]['thumb']); ?>" class="img-responsive center-block" />   
</div>
<div class="clearfix"></div>
<!-- FEATURES -->
<div id="features" class="features mbr-box mbr-section mbr-section--relative">
    <div class="container">
        <div class="row center">
            <div class="feature-item">
                <div class="col-md-3 col-sm-6">
                    <div class="item-head">
                        <i class="lnr lnr-diamond"></i>
                    </div>
                    <h6>creative design</h6>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- End features-item -->
            <div class="feature-item">
                <div class="col-md-3 col-sm-6">
                    <div class="item-head">
                        <i class="lnr lnr-rocket"></i>
                    </div>
                    <h6>well organized</h6>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.e</p>
                </div>
            </div>
            <!-- End features-item -->

            <div class="feature-item">
                <div class="col-md-3 col-sm-6">
                    <div class="item-head">
                        <i class="lnr lnr-mustache"></i>
                    </div>
                    <h6>easy to customize</h6>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- End features-item -->

            <div class="feature-item">
                <div class="col-md-3 col-sm-6">
                    <div class="item-head">
                        <i class="lnr lnr-phone"></i>
                    </div>
                    <h6>support 24/7</h6>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- End features-item -->
        </div>
    </div>
</div>
<section id="progetto" class="">
    <div class="container">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers progetto nexa-black"><?= $pageChildren[1]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[1]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[1]['content']; ?><br />
                    <?= $pageChildren[1]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PRICING -->
<section id="offerta" class="pricing">
    <div class="container">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers offerta nexa-black"><?= $pageChildren[2]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[2]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[2]['content']; ?><br />
                    <?= $pageChildren[2]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>

        <div class="row center">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing_plan">
                    <div class="plan_title"><h6>Standard</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
            <div class="numscroller" data-slno='1' data-min='0' data-max='29' data-delay='20' data-increment="5">29</div>
        </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Standard</strong> avrai la possibilità di dare visibilità alla tua azienda  tramite un profilo personale corredato di tutte le informazioni riguardo la tua attività, i luoghi ed i prodotti offerti</li>
                    </ul>
                    <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing_plan" style="background-color:#d6e0d1;">
                    <div class="plan_title"><h6>Plus</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
            <div class="numscroller" data-slno='1' data-min='0' data-max='79' data-delay='15' data-increment="5">79</div>
        </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Plus</strong> la nostra offerta si arricchisce rispetto alla Standard aggiungendo pubblicità sul portale tramite banner, pubblicità sui social  e la  possibilità di inserire un filmato sull’azienda.</li>
                    </ul>
                    <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing_plan">
                    <div class="plan_title"><h6>Full</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
            <div class="numscroller" data-slno='1' data-min='0' data-max='199' data-delay='13' data-increment="5">199</div>
        </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Full</strong> il format della tua azienda sul portale diventa dinamico, ed oltre alle funzioni del pacchetto Plus potrai usufruire di una completa personalizzazione del tuo profilo con l’inserimento di foto e video creati su misura per te.</li>
                    </ul>
                    <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CLIENTS -->
<div id="clients" class="clients">
    <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L50 100 L100 0 Z" fill="#fff" stroke="#fff"></path>
    </svg>
    <div class="container">
        <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div id="owl-demo">
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/1.png")?>" alt="Owl Image"></div>
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/2.png")?>" alt="Owl Image"></div>
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/3.png")?>" alt="Owl Image"></div>
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/4.png")?>" alt="Owl Image"></div>
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/2.png")?>" alt="Owl Image"></div>
                            <div class="item"><img src="<?= base_url(IMAGES."/clients/4.png")?>" alt="Owl Image"></div>
                    </div>
            </div>
    </div>
</div>

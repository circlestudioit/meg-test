<div class="zero">
    <div id="snack" class="col-xs-12 col-sm-12" style="background-image: url(<?=base_url(IMAGES."contatti.jpg")?>); background-size: cover; background-position: center; background-repeat: no-repeat">
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="col-xs-1 col-sm-2 col-md-2 col-lg-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-8 col-lg-6" id="contactcontent">
            <div class="col-xs-12 text-center">
                <h1 class="PF-BlackItalic dark bigger">
                    <?=$pagecontent['title']?>
                </h1>
                <h3 class="PF-Regular dark text-center">
                    <?=$pagecontent['headline']?>
                </h3>
            </div>
            <div class="col-xs-12 lato text-justify" style="margin: 3em auto">
                <?=$pagecontent['content']?>
            </div>
            <hr />
            <h3 class="PF-Regular green text-center">
                <?=$this->lang->line('nostro-stile');?>
            </h3>
            <form id="contattaci-form" method="post" action="#" style="margin: 2em auto">
                <input class="col-xs-12" type="text" id="nome" name="nome" placeholder="<?=$this->lang->line('nome')?>"><br>
                <input class="col-xs-12" type="text" id="company" name="company" placeholder="<?=$this->lang->line('company')?>"><br>
                <input class="col-xs-12" type="text" id="role" name="role" placeholder="<?=$this->lang->line('ruolo')?>"><br>
                <input class="col-xs-12" type="text" id="email" name="email" placeholder="<?=$this->lang->line('email')?>"><br>
                <br>
                <textarea style="color: #AAA" id="richieste" name="richieste" onfocus="if(this.value != '' ) this.value = ''"><?=$this->lang->line('scrivi-richiesta')?></textarea>
                <br>
<!--                <input id="privacy-check" name="privacy-check" type="checkbox" class="css-checkbox">
                <label for="privacy-check" name="checkbox_lbl" class="css-label">
                    <a href="<?=base_url("privacy-policy")?>" target="_blank" class="dark">
                    <?=$this->lang->line('privacy')?></a>
                </label>-->
                <br><br>
                <div id="form-messages" class="dark"></div>
                <br>
                <input id="submit" type="submit" value="<?=$this->lang->line('invia')?>">
            </form>
        </div>
        <div class="col-xs-1 col-sm-2 col-md-2 col-lg-3"></div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div id="map"></div>
    </div>
</div>
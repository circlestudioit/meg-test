<div class="page-background fill-half" style="background-image: url(<?=base_url($this->config->item('pages_cover').$pagecontent['cover'])?>)">
    <div class="interne-title">
        <h1 class="big white text-center">
            <?=$pagecontent['title']?>
        </h1>
    </div>
</div>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <?php $j=0; for($i = 0; $i < count($contenuto); $i+=4): ?>
            <?php if(isset($contenuto[$i])): ?>
            <div class="col-xs-12 col-sm-4 gallery-title-wrapper">
                <h1 class="LibreItalic big gold gallery-title">
                    <a class="neutral" href="<?=base_url("gallery/".$contenuto[$i]['info']['name'])?>">
                        <?=$contenuto[$i]['info']['title']?>
                    </a>
                </h1>
            </div>
            <div class="col-xs-12 col-sm-8 gallery-cover" style="background-image: url(<?=$this->config->item('photo_thumb').$contenuto[$i]['info']['thumb']?>); background-position: center; background-size: cover;"></div>
            <?php endif; ?>
            <?php if(isset($contenuto[$i+1])): ?>
            <div class="col-xs-12 col-sm-6 gallery-cover" style="background-image: url(<?=$this->config->item('photo_thumb').$contenuto[$i+1]['info']['thumb']?>); background-position: center; background-size: cover;"></div>
            <div class="col-xs-12 col-sm-6 gallery-title-wrapper">
                <h1 class="LibreItalic big gold gallery-title"><?=$contenuto[$i+1]['info']['title']?></h1>
            </div>
            <?php endif;?>
            <?php if(isset($contenuto[$i+2])): ?>
            <div class="col-xs-12 col-sm-6 gallery-title-wrapper">
                <h1 class="LibreItalic big gold gallery-title"><?=$contenuto[$i+2]['info']['title']?></h1>
            </div>
            <div class="col-xs-12 col-sm-6 gallery-cover" style="background-image: url(<?=$this->config->item('photo_thumb').$contenuto[$i+2]['info']['thumb']?>); background-position: center; background-size: cover;"></div>
            <?php endif;?>
            
            <?php if(isset($contenuto[$i+3])): ?>
            <div class="col-xs-12 col-sm-8 gallery-cover" style="background-image: url(<?=$this->config->item('photo_thumb').$contenuto[$i+3]['info']['thumb']?>); background-position: center; background-size: cover;"></div>
            <div class="col-xs-12 col-sm-4 gallery-title-wrapper">
                <h1 class="LibreItalic big gold gallery-title"><?=$contenuto[$i+3]['info']['title']?></h1>
            </div>
            <?php endif;?>

        <?php endfor; ?>
    </div>
</div>
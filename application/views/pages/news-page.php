<?php
    if($pagecontent['cover']!='')
        $background = base_url($this->config->item('pages_cover').$pagecontent['cover']);
    elseif($pagecontent['parent']['cover'] != '')
        $background = base_url($this->config->item('pages_cover').$pagecontent['parent']['cover']);
    else
        $background = base_url(IMAGES."interne-cover2.jpg");
?>
<div class="page-background fill-half" style="background-image: url(<?php echo $background ?>)">
    <div class="interne-title">
        <h1 class="big white text-center">
            <?=$pagecontent['title']?>
        </h1>
    </div>
</div>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <h1 class="big dark text-center" style="margin: 2em 0">
            <?=$pagecontent['headline']?>
        </h1>
        <div class="col-xs-12 col-sm-3 submenu-page events-page">
            <div class="col-xs-12">
                <div class="brown-line-full"></div>
                <h3 class="ClanMedium dark">
                    In evidenza
                </h3>
            </div>
            <div class="col-xs-12">
                <h4>
                    <a href="<?= base_url('news/'.$contenuto['highlight_post']['url']);?>">
                        <?= $contenuto['highlight_post']['title']; ?>
                    </a>
                </h4>
                <?php if($contenuto['highlight_post']['thumb'] != ''): ?>
                    <a class="img-opacity" href="<?= base_url('news/'.$contenuto['highlight_post']['url']);?>">
                        <img src="<?=base_url($this->config->item('post_thumb').$contenuto['highlight_post']['thumb']);?>" class="img-responsive" />
                    </a>
                <?php endif; ?>
            </div>
            <div class="clear" style="margin: 3em 0;">&nbsp;</div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <section class="col-xs-12 col-sm-12">
                
                <?php foreach($contenuto['next_event'] as $post): ?>
                <div class="text-left ClanMedium">
                    <a href="<?=base_url("news/".$post['url'])?>">
                        <p class="ClanMedium text-justify">
                            <?=$post['title'] ?>
                        </p>
                    </a>
                    <div class="ClanBook dark text-justify" style="margin-bottom: 2em"><?=word_limiter($post['content'], 15);?></div>
                </div>
                <div class="clear event-line"></div>

                <?php endforeach; ?>
            </section>
        </div>
    </div>
</div>
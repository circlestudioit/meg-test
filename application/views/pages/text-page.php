<div class="hidden-xs col-sm-3"></div>
<div class="col-xs-12 col-sm-6" style="margin-top: 5em">
    
    <h1 class="text-center dark playfairib bigger"><?=$pagecontent['title']?></h1>
    <?php if($pagecontent['headline'] != ''):?>
    <h2 class="text-center quickpen dark">
        <?=$pagecontent['headline']?>
    </h2>
    <?php endif; ?>
    
    <div class="col-xs-12 lato text-justify" style="margin: 3em auto">
        <?=$pagecontent['content']?>
    </div>
</div>
<div class="hidden-xs col-sm-3"></div>
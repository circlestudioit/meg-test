<div class="form-container">
    <form id="open-pizzeria-form" enctype="multipart/form-data" method="post" action="home/send_franchising_request_mail">
        <h3 class="cardo">
            <?=$this->lang->line('dati-anagrafici')?>
        </h3>
        
        <div>    
            <input type="text" class="col-xs-12 col-sm-6" name="last_name" placeholder="<?=$this->lang->line('cognome')?>" required>
            <input type="text" class="col-xs-12 col-sm-6" name="first_name" placeholder="<?=$this->lang->line('nome')?>" required>&nbsp;&nbsp;
            <p>
                <?=$this->lang->line('sesso')?>:&nbsp;&nbsp;M&nbsp;
                <input type="radio" name="sex" id="sex" value="M" required />&nbsp;&nbsp;
                F&nbsp;&nbsp;<input type="radio" name="sex" id="sex" value="F" required />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?=$this->lang->line('eta')?>&nbsp;&nbsp;

                <select name="age" id="age" required>
                   <?php for($i = 18; $i <= 100; $i++): ?>
                                <option value="<?=$i?>"><?=$i?></option>
                   <?php endfor; ?>
                </select>
            </p>
        </div>
        <div class="clear"></div>
        <div>
            <input class="col-xs-12 col-sm-9" name="address" type="text" id="address" placeholder="<?=$this->lang->line('indirizzo')?>" required />
            <input class="col-xs-12 col-sm-3" name="zip" type="text" id="zip" placeholder="<?=$this->lang->line('cap')?>" required />
            <input class="col-xs-12 col-sm-6" name="city" type="text" id="city" placeholder="<?=$this->lang->line('citta')?>" required />
            <input class="col-xs-12 col-sm-6" name="province" type="text" id="province" placeholder="<?=$this->lang->line('provincia')?>" required />
            <div class="clear"></div>
            <input class="col-xs-12 col-sm-4" name="cellphone" type="text" id="cellphone" placeholder="<?=$this->lang->line('cellulare')?>" required />
            <input class="col-xs-12 col-sm-4" name="phone" type="text" id="phone" placeholder="<?=$this->lang->line('telefono')?>" />
            <input class="col-xs-12 col-sm-4" name="email" type="text" id="email" placeholder="<?=$this->lang->line('email')?>" required />
        </div>
        
        <div class="clear"></div>

        <div>
            <input class="col-xs-12 col-sm-12" name="titolo-di-studio" type="text" id="titolo-di-studio" placeholder="<?=$this->lang->line('titolo-di-studio')?>" />
            <br /><br /><br /><br />
            <p><?=$this->lang->line('azienda')?></p>
            <p style="float: left"><?=$this->lang->line('si')?>&nbsp;&nbsp;</p><input type="radio" name="azienda" id="azienda" value="Y" style="float: left; margin-right: 20px" />
            <p style="float: left"><?=$this->lang->line('no')?>&nbsp;&nbsp;</p><input type="radio" name="azienda" id="azienda" value="N" style="float: left" />
            <div class="clear"></div>
            <input class="col-xs-12 col-sm-12" name="ragione-sociale" type="text" id="ragione-sociale" placeholder="<?=$this->lang->line('ragione-sociale')?>" />
        </div>

        <div class="clear"></div>
        <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
        
        <div>
            <h3 class="cardo">
                <?=$this->lang->line('per-la-tua-pizzeria')?>
            </h3>
            <br />
            <p>
                <?=$this->lang->line('disponibilita-locale')?>&nbsp;&nbsp;
            </p>
            <p style="float: left; margin-right: 10px">
                <?=$this->lang->line('si')?>&nbsp;&nbsp;
                <input type="radio" name="disponibilita-locale" id="disponibilita-locale" value="SI" />
            </p>
            <p>
                <?=$this->lang->line('no')?>&nbsp;&nbsp;<input type="radio" name="disponibilita-locale" id="disponibilita-locale" value="NO" />
            </p>
            <input class="col-xs-12 col-sm-12" disabled="disabled" name="indirizzo-locale" type="text" id="indirizzo-locale" placeholder="<?=$this->lang->line('indirizzo')?>" />
            <div class="col-xs-12 col-sm-6" style="margin-top: 3em">          
                <p>
                    <?=$this->lang->line('nazione-di-interesse')?>
                </p>
                <select class="col-xs-12 col-sm-12" name="nazione-di-interesse" id="nazione-di-interesse">
                    <?php foreach($all_countries as $country): ?>   
                        <option value="<?=$country['country_code']?>"><?=$country['country_name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-6" style="margin-top: 3em">
                <p>
                    <?=$this->lang->line('provincia-di-interesse')?>
                </p>
                <select class="col-xs-12 col-sm-12" name="provincia-di-interesse" id="provincia-di-interesse">
                    <?php foreach($province as $provincia): ?>    
                        <option value="<?=$provincia['sigla']?>"><?=$provincia['sigla']?></option>
                    <?php endforeach; ?>
                </select>
            </div>	


        </div>
        
        <div class="clear"></div>
        <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
        
        <h3 class="cardo">
            <?=$this->lang->line('modalita-finanziamento')?>
        </h3>
        <p class="col-xs-12 col-sm-12">
        <input type="radio" name="mezzi-finanziari" id="mezzi-finanziari" value="mezzi-finanziari-propri" />&nbsp;&nbsp;<?=$this->lang->line('mezzi-finanziari-propri')?>
        </p>
        <p class="col-xs-12 col-sm-12">
        <input type="radio" name="mezzi-finanziari" id="mezzi-finanziari" value="mezzi-finanziari-terzi" />&nbsp;&nbsp;<?=$this->lang->line('mezzi-finanziari-terzi')?>
        </p>
        <div class="clear"></div>
        <br /><br />
        <p>
            <?=$this->lang->line('note')?>&nbsp;&nbsp;
        </p>
        <textarea name="note" id="note"></textarea>
        <br />
        <br />
        <p><?=$this->lang->line('orari-contatto')?>&nbsp;&nbsp;
        <select name="orari-contatto" id="orari-contatto">
            <option value="mattina"><?=$this->lang->line('mattina')?></option>
            <option value="pomeriggio"><?=$this->lang->line('pomeriggio')?></option>
            <option value="sera"><?=$this->lang->line('sera')?></option>

        </select>
        </p>
        
        <div class="clear"></div>
        <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
        
        <h3 class="cardo">
            <?=$this->lang->line('documenti-da-allegare')?>
        </h3>
        
        <p>
            <?=$this->lang->line('doc-aziende')?><br />
            <?=$this->lang->line('doc-investitori')?>
        </p>
        <input type="file" id="upload" name="upload" />
        
        <div class="clear"></div>
        <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
            
        <h3 class="cardo">
            <?=$this->lang->line('privacy')?>
        </h3>
        <p><?=$this->lang->line('privacy-txt')?>&nbsp;&nbsp;</p>
        <input type="checkbox" name="accept-privacy" id="accept-privacy" value="privacy-ok" required />&nbsp;<br />
        <p>
           <?=$this->lang->line('privacy-dichiaro')?>
        </p>
        
        <div class="clear"></div>
        
        <input type="submit" value="<?=$this->lang->line('invia-richiesta')?>" onclick="send_franchising_mail()" />
    </form>
    
    <p id="franchising-form-success" class="text-center">
                
    </p>
    
</div>
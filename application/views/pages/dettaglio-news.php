<div class="page-background fill-half" style="background-image: url(<?=base_url(IMAGES."interne-cover2.jpg")?>);)">
    <div class="interne-title">
        <h1 class="big white text-center">
            NEWS
        </h1>
    </div>
</div>


<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <div class="col-xs-12 col-sm-9 col-sm-offset-3 ">
            <h1 class="big dark text-left" style="margin: 1em 0 .25em 0">
               <?=$pagecontent['title']?>
            </h1>
            <?php if($pagecontent['headline'] != ''): ?>
            <h2 class="dark text-left" style="margin-top: 0;">
                <?=$pagecontent['headline']?>
            </h2>
            <?php endif; ?>
        </div>
        <?php 
        setlocale(LC_TIME, strtolower($this->session->userdata('lang'))."_".strtoupper($this->session->userdata('lang')));

        $data['day'] = strftime("%d", strtotime($pagecontent['date']));
        $data['month'] = strftime("%B", strtotime($pagecontent['date']));
        $data['year'] = strftime("%Y", strtotime($pagecontent['date']));
        ?>
        <div class="col-xs-12 col-sm-12 text-right">
            <?=$data['day']?> <?=$data['month']?> <?=$data['year']?>
        </div>
        <div class="col-xs-12 col-sm-12">
            <div class="brown-line-full1" style="margin: 1em 0 2em 0"></div>
        </div>
        <div class="col-xs-12 col-sm-3 submenu-page">
            <?php if(!empty($pagecontent['gallery'])): ?>
                    <div class="text-left">Photogallery</div>
                    <div class="brown-line-full1" style="margin: .5em 0 2em 0"></div>
                <?php $i = 0; foreach ($pagecontent['gallery'] as $images): ?>
    <!--                <div class="col-xs-6">-->
                         <div class="imagepreview" style="width: 50%; height: 100px; float: left; background-image: url(<?=base_url($this->config->item('photo_image').$images['image']); ?>)">
                            <a href="<?=base_url($this->config->item('photo_image').$images['image']); ?>" data-lity></a>
                        </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if(!empty($pagecontent['evento'])): ?>
                    <div class="clear"  style="margin: 2em 0">&nbsp;</div>
                    <div class="text-left">Evento correlato</div>
                    <div class="brown-line-full1" style="margin: .5em 0 2em 0"></div>
                    <a href="<?= base_url('evento/'.$pagecontent['evento']['url']); ?>">
                        <?= $pagecontent['evento']['title']; ?>
                    </a>
                    <div class="clear"  style="margin: 1em 0">&nbsp;</div>
            <?php endif; ?>
            
            <div class="text-left">Archivio News</div>
            <div class="brown-line-full1" style="margin: .5em 0 2em 0"></div>
            <a href="<?= base_url('news')?>">
                Vai all'Archivio
            </a>
            
            
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php if($pagecontent['thumb'] != ''): ?>
                <img class="img-responsive center-block" src="<?=base_url($this->config->item('post_thumb').$pagecontent['thumb']); ?>" />
            <?php endif; ?>
            <div class="text-justify" style="margin: 2em 0">
                <?=$pagecontent['content']?> 
            </div>
        </div>
    </div>
</div>
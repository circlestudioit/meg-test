<div class="page-background fill-half" style="background-image: url(<?=base_url($this->config->item('photo_thumb').$pagecontent['gallery_info']['thumb'])?>)">
    <div class="interne-title">
        <h1 class="big white text-center">
            <?=$pagecontent["gallery_info"]["title"]?>
        </h1>
    </div>
</div>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <?php foreach($pagecontent['gallery_photos'] as $photo): //print_r($photo); ?>
            <img src="<?=base_url($this->config->item('photo_image').$photo['image'])?>" class="img-responsive gallery-thumb" />
        <?php endforeach; ?>
    </div>
</div>
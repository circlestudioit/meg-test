
<div id="slider-hp" class="flexslider col-xs-12 col-sm-12">
    <ul class="slides" style="height:90%;">
        <?php foreach($gallery as $pic): ?>
            <li class="slide" width="100%">
                <img src="<?= base_url($this->config->item('photo_image').$pic['image']); ?>" class="sliderimage"  width="1920" heigth="1080" />
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="clear"></div>
<div id="" class="container-fluid">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4 col-xl-4 col-sm-offset-2 col-md-offset-2 col-lg-offset-4 col-xl-offset-4"  style="padding: 6em 0;">
        <img src="<?= base_url($this->config->item('pages_thumb').$pagecontent['thumb']); ?>" class="img-responsive center-block" />
        <h2 class="text-center"><?=$pagecontent['title']?></h2>
    </div>
</div>
<div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5">
    <img src="<?= base_url(IMAGES."break/".$pageChildren[0]['headline'].".svg"); ?>" class="img-responsive center-block" />
</div>
<section id="meg" class="team space">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers meg nexa-black"><?= $pageChildren[0]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[0]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[0]['content']; ?>
                    <?= $pageChildren[0]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
        <img src="<?= base_url($this->config->item('pages_thumb').$pageChildren[0]['thumb']); ?>" class="img-responsive center-block" />   
    </div>
</div>
<div class="clearfix"></div>
<div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5 space-top">
    <img src="<?= base_url(IMAGES."break/".$pageChildren[1]['headline'].".svg"); ?>" class="img-responsive center-block" />
</div>
<section id="progetto" class="space">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers progetto nexa-black"><?= $pageChildren[1]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[1]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[1]['content']; ?>
                    <?= $pageChildren[1]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
    </div>
</section>
<div class="col-xs-4 col-xs-offset-4">
    <img src="<?= base_url($this->config->item('pages_thumb').$pageChildren[1]['thumb']); ?>" class="img-responsive center-block" />   
</div>
<div class="clearfix"></div>
<!-- PRICING -->
<div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5 space-top">
    <img src="<?= base_url(IMAGES."break/".$pageChildren[2]['headline'].".svg"); ?>" class="img-responsive center-block" />
</div>
<section id="offerta" class="pricing space">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers offerta nexa-black"><?= $pageChildren[2]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[2]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[2]['content']; ?>
                    <?= $pageChildren[2]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="pricing_plan">
                    <div class="plan_title"><h6>standard</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
                        <div class="numscroller" data-slno='1' data-min='0' data-max='500' data-delay='13' data-increment="5">500</div>
                    </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Standard</strong> avrai la possibilità di dare visibilità alla tua azienda  tramite un profilo personale corredato di tutte le informazioni riguardo la tua attività, i luoghi ed i prodotti offerti.</li>
                    </ul>
                    <!-- <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>-->
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="pricing_plan" style="background-color:#d6e0d1;">
                    <div class="plan_title"><h6>medium</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
                        <div class="numscroller" data-slno='1' data-min='0' data-max='800' data-delay='15' data-increment="5">800</div>
                    </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Plus</strong> la nostra offerta si arricchisce rispetto alla Standard aggiungendo pubblicità sul portale tramite banner, pubblicità sui social  e la  possibilità di inserire un filmato sull’azienda.<br /><br /></li>
                    </ul>
                    <!-- <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>-->
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="pricing_plan">
                    <div class="plan_title"><h6>premium</h6></div>
                    <div class="plan_price">
                        <i class="fa fa-eur" aria-hidden="true"></i>
                        <div class="numscroller" data-slno='1' data-min='0' data-max='1200' data-delay='20' data-increment="5">1200</div>
                    </div>
                    <ul class="list">
                        <li>Con il pacchetto <strong>Full</strong> il format della tua azienda sul portale diventa dinamico, ed oltre alle funzioni del pacchetto Plus potrai usufruire di una completa personalizzazione del tuo profilo con l’inserimento di foto e video creati su misura per te.</li>
                    </ul>
                    <!-- <a href="" class="default-btn"> See More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>-->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5 space-top">
    <img src="<?= base_url(IMAGES."break/".$pageChildren[3]['headline'].".svg"); ?>" class="img-responsive center-block" />
</div>
<section id="buyers" class="space">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers buyers nexa-black"><?= $pageChildren[3]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[3]['title']; ?></h2>
                    <section class="module-subtitle">
                    <div class="text-center"><?= $pageChildren[3]['content']; ?></div>
                    <?= $pageChildren[3]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
    </div>
</section>
<div class="col-xs-8 col-xs-offset-2">
    <img src="<?= base_url($this->config->item('pages_thumb').$pageChildren[3]['thumb']); ?>" class="img-responsive center-block col-xs-12" />   
</div>
<div class="clearfix"></div>

<div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5 space-top">
    <img src="<?= base_url(IMAGES."break/".$pageChildren[4]['headline'].".svg"); ?>" class="img-responsive center-block" />
</div>
<section id="contatti" class="space">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-3 col-xl-offset-3">
            <div class=" text-center">
                <div class="section-title-parralax">
                    <div class="process-numbers contatti nexa-black"><?= $pageChildren[4]['headline']; ?></div>
                    <h2 class=" nexa-black"><?= $pageChildren[4]['title']; ?></h2>
                    <section class="module-subtitle">
                    <?= $pageChildren[4]['content']; ?>
                    <?= $pageChildren[4]['content_2']; ?>
                    </section> 
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-xl-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xl-offset-1">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="">
                    <div class="plan_title"><h6>richedi informazioni</h6></div>
                    <div class="contact-form bottom">
                        <form id="main-contact-form" name="contact-form" method="post" action="#">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="nome" id="nome" class="form-control" required="required" placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <input type="text" name="azienda" id="azienda" class="form-control" required="required" placeholder="Azienda">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" required="required" placeholder="E-Mail">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Messaggio"></textarea>
                            </div>
                            <div class="form-group">
                                <button id="submit_btn" class="default-btn gray-btn pull-right"> Invia </button>
                            </div>
                        </div>
                        </form>
                        <div id="form-messages"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="">
                        <div id="map" style="width:100%;height:450px"></div>
                        <script>
                        function myMap() {
                          var mapCanvas = document.getElementById("map");
                          var myCenter = new google.maps.LatLng(44.394559, 7.550977); 
                          var mapOptions = {center: myCenter, zoom: 17};
                          var map = new google.maps.Map(mapCanvas,mapOptions);
                          var marker = new google.maps.Marker({
                            position: myCenter,
                            icon: "<?= IMAGES.'google-pin.png' ?>"
                          });
                          marker.setMap(map);
                        }
                        </script>

                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAAtNoqSMLYEEdgSi77Z2Z5DUWFOJaJHs&callback=myMap"></script>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="plan_title col-xs-12"><h6>come arrivare</h6></div>
            
            <div class="col-xs-12 col-sm-12 col-md-4 module-subtitle">
                        <strong>IN AUTO</strong>
                    <p>
                        <strong>A21 Torino-Piacenza</strong><br />
                        uscita Asti Est e proseguimento su A33 Asti-Cuneo con uscita a Sant&rsquo;Albano Stura<br />
                        Tel. +39.011.4373800<br />
                        <a href="http://www.a21torinopiacenza.it" target="_blank">www.a21torinopiacenza.it</a></p>
                    <p>
                        <strong>A6 Torino- Savona</strong><br />
                        con proseguimento su A33 Asti-Cuneo ed uscita a Sant&rsquo;Albano Stura<br />
                        Tel. +39.011.6650311<br />
                        <a href="http://www.tosv.it" target="_blank">www.tosv.it</a></p>
                    <p>
                        <strong>A33 Asti-Cuneo</strong>&nbsp;(in fase di completamento)<br />
                        uscita Cuneo<br />
                        <a href="http://www.asticuneo.it" target="_blank">www.asticuneo.it</a></p>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 module-subtitle">
                    <strong>IN TRENO</strong>
                    <p>
                        Le stazioni ferroviarie principali si trovano a Cuneo, Fossano, Savigliano, Mondov&igrave; e Saluzzo.</p>
                    <p>
                        <strong>Trenitalia</strong><br />
                        Tel. Call Center FS 892021<br />
                        <a href="http://www.trenitalia.com" target="_blank">www.trenitalia.com</a></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 module-subtitle">
                   <strong>IN AEREO</strong>
                    <p>
                        <strong>Aeroporto Internazionale di Torino Caselle</strong><br />
                        Str. San Maurizio 12 &ndash; 10072 Caselle Torinese (TO)<br />
                        Tel. +39.011.5676361 &ndash; Fax +39.011.5676420<br />
                        <a href="http://www.aeroportoditorino.it" target="_blank">www.aeroportoditorino.it</a></p>
                    <p>
                        <strong>Aeroporti di Milano (Malpensa e Linate)</strong><br />
                        Tel. Call center SEA +39.02.232323<br />
                        <a href="http://www.sea-aeroportimilano.it" target="_blank">www.sea-aeroportimilano.it</a></p>
                    <p>
                        <strong>Aeroporto di Genova &ldquo;C. Colombo&rdquo;</strong><br />
                        16154 Genova<br />
                        Tel. +39.010.60151<br />
                        <a href="http://www.airport.genova.it" target="_blank">www.airport.genova.it</a></p>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</section>
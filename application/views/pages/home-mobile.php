<div class="homepage-background fill" style="background-image: url(<?=base_url($this->config->item('photo_image').$pagecontent['featph']['image'])?>)">
    <div class="home-title-mobile bottomTopOpenDelay" style="padding-left: 15px; padding-right: 15px;">
        <h1 class="white text-center">
             <?=$pagecontent['title']?>
         </h1>
        <br /><br />
        <p class="ClanBook white text-center col-xs-12">
            Stiamo migliorando l'esperienza per il tuo Smartphone.<br />
            Oggi ti consigliamo di navigare dal tuo Computer.
        </p>
    </div>
</div>

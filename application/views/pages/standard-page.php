<?php
    if($pagecontent['cover']!='')
        $background = base_url($this->config->item('pages_cover').$pagecontent['cover']);
    elseif($pagecontent['parent']['cover'] != '')
        $background = base_url($this->config->item('pages_cover').$pagecontent['parent']['cover']);
    else
        $background = base_url(IMAGES."interne-cover2.jpg");
?>
<div class="page-background fill-half" style="background-image: url(<?php echo $background ?>)">
    <div class="interne-title">
        <h1 class="big white text-center">
            <?=$pagecontent['title']?>
        </h1>
    </div>
</div>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <div class="col-xs-12 col-sm-9 col-sm-offset-3 ">
            <h1 class="big dark text-left" style="margin: 2em 0">
                <?=$pagecontent['headline']?>
            </h1>
        </div>
        <div class="hidden-xs col-sm-3 col-md-3 col-lg-3 col-xl-3 submenu-page">
            <div class="brown-line-full"></div>
            <h3 class="ClanMedium dark">
                <?php
                if(!empty($pagecontent['parent'])) echo $pagecontent['parent']['title']; else echo $pagecontent['title']; ?>
            </h3>
            <ul class="submenu-page-ul">
                <?php foreach ($navigation as $nav): ?>
                   <li>
                    <a href="<?= base_url($navigation_parent['url'].'/'.$nav['url']); ?>"><?= $nav['title']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
            <section>
                <div class="page-intro dark text-justify">
                    <?=$pagecontent['content']?>
                </div>
                <div class="page-text dark text-justify">
                    <?=$pagecontent['content_2']?>
                </div>
            </section>
        </div>
    </div>
</div>
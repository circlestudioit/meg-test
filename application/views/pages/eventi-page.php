<?php
    if($pagecontent['cover']!='')
        $background = base_url($this->config->item('pages_cover').$pagecontent['cover']);
    elseif($pagecontent['parent']['cover'] != '')
        $background = base_url($this->config->item('pages_cover').$pagecontent['parent']['cover']);
    else
        $background = base_url(IMAGES."interne-cover2.jpg");
?>
<div class="page-background fill-half" style="background-image: url(<?php echo $background ?>)">
    <div class="interne-title">
        <h1 class="big white text-center">
            <?=$pagecontent['title']?>
        </h1>
    </div>
</div>
<div class="container-fluid">
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-xl-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2 col-xl-offset-2" style="margin-bottom: 2em">
        <h1 class="big dark text-center" style="margin: 2em 0">
            <?=$pagecontent['headline']?>
        </h1>
        <div class="col-xs-12 col-sm-3 submenu-page events-page">
            <div class="col-xs-12">
                <div class="brown-line-full"></div>
                <h3 class="ClanMedium dark">
                    Il prossimo evento
                </h3>
            </div>
            <div class="col-xs-12">
                <h4>
                    <a href="<?= base_url('eventi/'.$contenuto['next_event'][0]['url']);?>">
                        <?= $contenuto['next_event'][0]['title']; ?>
                    </a>
                </h4>
                <?php if($contenuto['next_event'][0]['thumb'] != ''): ?>
                    <a class="img-opacity" href="<?= base_url('eventi/'.$contenuto['next_event'][0]['url']);?>">
                        <img src="<?=base_url($this->config->item('post_thumb').$contenuto['next_event'][0]['thumb']);?>" class="img-responsive" />
                    </a>
                <?php endif; ?>
            </div>
            <div class="clear" style="margin: 3em 0;">&nbsp;</div>
            <div class="col-xs-12">
                <div class="brown-line-full"></div>
                <h3 class="ClanMedium dark">
                    L'ultimo evento
                </h3>
            </div>
            <div class="col-xs-12">
                <h4>
                    <a href="<?= base_url('eventi/'.$contenuto['gallery_last_event']['url']);?>">
                        <?= $contenuto['gallery_last_event']['title']; ?>
                    </a>
                </h4>
                <?php if($contenuto['gallery_last_event']['thumb'] != ''): ?>
                    <a class="img-opacity" href="<?= base_url('eventi/'.$contenuto['gallery_last_event']['url']);?>">
                        <img src="<?=base_url($this->config->item('post_thumb').$contenuto['gallery_last_event']['thumb']);?>" class="img-responsive" />
                    </a>
                <?php endif; ?>
            </div>
            <div class="clear" style="margin: 1em 0;">&nbsp;</div>
<!--            <div class="col-xs-6">
                    <a href="<?= base_url('gallery/');?>">
                    Gallery
                    </a>
            </div>-->
            <div class="col-xs-12 col-sm-12 text-left">    
                <div class="brown-line-full"></div>
                <a href="<?= base_url('archivio/');?>">
                    <h4 class="ClanBook dark">
                        Archivio eventi
                    </h4>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="col-xs-12 col-sm-12">
                <h2 class="ClanBook dark">
                    In Calendario
                </h2>
            <?php if(!empty($contenuto['next_event'])){ ?>
                <div class="col-xs-12 col-sm-12">
                    
                    <?php 
                    $date = mktime(0, 0, 0, date('n'), 1, date('Y'));
                    $curmonth = strftime( '%B', strtotime( 'now', $date));
                    ?>
                    <h4 class="dark ClanMedium"><?=strtoupper($curmonth); ?></h4>
                    <?php $days = cal_days_in_month ( CAL_GREGORIAN , date('n', strtotime('first day of month')) , date('Y'));
                    for($i = 1; $i<=$days; $i++): ?>
                    <div class="text-left">
                        
                        <?php   
                            foreach($contenuto['next_event'] as $post): 
                                $data['day'] = strftime("%d", strtotime($post['date']));
                                $data['month'] = strftime("%B", strtotime($post['date']));
                                $data['year'] = strftime("%Y", strtotime($post['date']));
                        ?>
                        <?php if($data['month'] == $curmonth && $data['day'] == $i): ?>

                            <div class="text-left ClanMedium ">
                                <a href="<?= base_url('eventi/'.$post['url']); ?>" class="dark">
                                    <span class="event-icon"><?=$i?>&nbsp;&nbsp;</span>
                                    <p class="ClanMedium event-title text-justify">
                                        <?=$post['title']?>
                                    </p>
                                </a>
                            </div>
                            <div class="clear event-line"></div>
                        <?php endif; endforeach; ?>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="clear" style="margin: 2em 0;">&nbsp;</div>
                <?php if(isset($contenuto['next_event'][1])): ?>
                    <?php for($z = 1; $z<=count($contenuto['next_event']-1); $z++):?>
                        <div class="col-xs-12 col-sm-12">
                            <?php 
                            $date = mktime(0, 0, 0, date('n'), 1, date('Y'));
                            $nextmonth = strftime( '%B', strtotime( '+ '.$z.' month', $date));
                            ?>
                            <h4 class="dark ClanMedium"><?=strtoupper($nextmonth); ?></h4>
                            <?php $days = cal_days_in_month ( CAL_GREGORIAN , date('n', strtotime('first day of next month')) , date('Y'));
                            for($i = 1; $i<=$days; $i++): ?>
                            <div class="postagenda text-left">

                                <?php   
                                    foreach($contenuto['next_event'] as $post): 
                                        $data['day'] = strftime("%d", strtotime($post['date']));
                                        $data['month'] = strftime("%B", strtotime($post['date']));
                                        $data['year'] = strftime("%Y", strtotime($post['date']));
                                ?>
                                <?php if($data['month'] == $nextmonth && $data['day'] == $i): ?>
                                    <div class="text-left ClanMedium ">
                                        <a href="<?= base_url('eventi/'.$post['url']); ?>" class="dark">
                                            <span class="event-icon"><?=$i?>&nbsp;&nbsp;</span>
                                            <p class="ClanMedium event-title text-justify">
                                                <?=$post['title']?>
                                            </p>
                                        </a>
                                    </div>
                                <div class="clear event-line"></div>
                                <?php endif; endforeach; ?>
                            </div>
                            <?php endfor; ?>
                        </div>
                        <div class="clear" style="margin: 2em 0;">&nbsp;</div>
                    <?php endfor; ?>
                <?php endif; ?>
            <?php }else{ ?>
                <div class="col-xs-12 col-sm-12">
                  <h4 class="dark ClanMedium"><?= $this->lang->line('no-evento'); ?></h4>
                </div>
            <?php } ?>    
            </div>

        </div>
    </div>
</div>
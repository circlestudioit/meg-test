<?php

$lang['ops']                                = "Oops! There's been an error";
$lang['page-not-found']                     = "Page not found. Make sure it's been created in the CMS.";

?>

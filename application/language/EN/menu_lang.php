<?php

$lang['home']                             = "HOME";
$lang['lavori']                           = "WORKS";
$lang['cosa-facciamo']                    = "WHAT WE DO";
$lang['eventi']                           = "EVENTS";
$lang['riconoscimenti']                   = "AWARDS";
$lang['contatti']                         = "CONTACT";

$lang['sitemap']                          = "Mappa del sito";

$lang['IT'] = "ITA";
$lang['EN'] = "ENG";
$lang['FR'] = "FRA";

$lang['credits'] = "Credits";
?>

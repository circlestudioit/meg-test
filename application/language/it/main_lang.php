<?php


/* AZIENDA */
// range di produzione bottiglie annua 
$lang['range_1']		= "fino a 100.000 bottiglie";
$lang['range_2']		= "fra 100.000 e 250.000 bottiglie";
$lang['range_3']		= "fra 250.000 e 500.000 bottiglie";
$lang['range_4']		= "oltre 500.000 bottiglie";

$lang['esplora']                            = "Esplora";
$lang['alt_logo_azienda']					= "Logo Azienda";
$lang['contattaci']                         = "Contattaci";
$lang['search']                             = "Cerca per Nome";


$lang['contenitore-di-idee']                = "Contenitore di idee";
$lang['subtitle-home']                      = "Intuizioni e capacità tecniche per creare prodotti digitali.<br />Sì, facciamo anche le brochure. E non solo.";
$lang['guarda-il-video']                    = "GUARDA IL VIDEO";
$lang['showreel']                           = "SHOWREEL";
$lang['chiedi-informazioni']                = "CHIEDI INFORMAZIONI";
$lang['i-nostri-eventi']                    = "I NOSTRI EVENTI";
$lang['vai-al-progetto']                    = "VAI AL PROGETTO";
 
$lang['progetto-per']                       = "Il nostro progetto per";
$lang['cliente']                            = "Cliente";
$lang['progetto']                           = "Progetto";
$lang['tutti-i-lavori']                     = "TUTTI I LAVORI";

$lang['foto-eventi-title']                  = "Alcune foto dei nostri eventi";

$lang['grazie-per-aver-inviato']            = "Grazie per aver inviato la tua richiesta";

$lang['leggi-tutto']                        = "LEGGI TUTTO";
$lang['scopri']                             = "SCOPRI";
$lang['scopri-storia']                      = "SCOPRI QUESTA STORIA";


$lang['nostro-stile']                       = "Ti piace il nostro stile? Lavora con noi!";
$lang['nome']                               = "NOME *";
$lang['cognome']                            = "COGNOME *";
$lang['oggetto']                            = "OGGETTO *";
$lang['nazione']                            = "NAZIONE *";
$lang['company']                            = "AZIENDA*";
$lang['ruolo']                              = "IL TUO RUOLO*";
$lang['email']                              = "INDIRIZZO EMAIL *";
$lang['scrivi-richiesta']                   = "MESSAGGIO *";
$lang['privacy']                            = "Ho letto e Accetto l'informativa sulla privacy";
$lang['invia']                              = "INVIA";

$lang['spiacenti']                          = "Spiacenti, nessuna immagine è stata caricata per questo lavoro.";


$lang['no-evento']							= "Nessun evento in programma";

?>
